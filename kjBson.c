// 
// FILE            kjBson.c - convert a KjNode tree into a BSON object
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // printf
#include <string.h>                     // memset
#include <bson.h>                       // BSON

#include "kbase/kMacros.h"              // K_FT, et al
#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "kbase/kLibLog.h"              // K Log macros

#include "kjson/KjTraceLevels.h"        // Trace Levels for the kjson library
#include "kjson/kjson.h"                // Kjson struct
#include "kjson/kjBson.h"               // Own Interface



// -----------------------------------------------------------------------------
//
// kjBson2 -
//
static bool kjBson2(bson_t* container, bson_t* childP, KjNode* kNodeP, char** detailsP)
{
  bson_t* objectP = NULL;
  bson_t* arrayP  = NULL;
  bson_t* memberP = NULL;

  printf("\nIn kjBson2: %s\n", kNodeP->name);

  switch (kNodeP->type)
  {
  case KjObject:
    printf("Got an object\n");
    objectP = (bson_t*) malloc(sizeof(bson_t));  // Create the BSON Object
    if (objectP == NULL)
    {
      *detailsP = "out of memory";
      return false;
    }
    bson_init(objectP);

    if (container != NULL)
    {
      printf("Appending JSON Object '%s'\n", kNodeP->name);
      printf("Calling bson_append_document_begin\n");
      bson_append_document_begin(container, kNodeP->name, -1, objectP);
      printf("Calling bson_append_document\n");
      bson_append_document(container, kNodeP->name, -1, objectP);  // Adding the BSON Object to its container
      bson_append_document_end(container, objectP);
      printf("Calling bson_append_document_end\n");
      printf("Appended JSON Object '%s'\n", kNodeP->name);
    }
    else
      printf("Container is NULL, so this is the top-level doc\n");
    break;

  case KjArray:
    arrayP = (bson_t*) malloc(sizeof(bson_t));  // Create the BSON Array
    if (arrayP == NULL)
    {
      *detailsP = "out of memory";
      return false;
    }
    bson_init(arrayP);

    if (container != NULL)
    {
      printf("Appending JSON Array '%s'\n", kNodeP->name);
      bson_append_document_begin(container, kNodeP->name, -1, arrayP);
      bson_append_array(container, kNodeP->name, -1, arrayP);  // Adding the BSON Array to its container
      bson_append_document_end(container, arrayP);
    }
    break;

  case KjString:
    bson_append_document_begin(container, kNodeP->name, -1, childP);
    bson_append_symbol(childP, kNodeP->name, -1, kNodeP->value.s, -1);
    bson_append_document_end(container, childP);
    break;

  case KjNull:
    bson_append_document_begin(container, kNodeP->name, -1, childP);
    bson_append_null(childP, kNodeP->name, -1);
    bson_append_document_end(container, childP);
    break;

  case KjInt:
    bson_append_document_begin(container, kNodeP->name, -1, childP);
    bson_append_int64(childP, kNodeP->name, -1, kNodeP->value.i);
    bson_append_document_end(container, childP);
    break;

  case KjBoolean:
    bson_append_document_begin(container, kNodeP->name, -1, childP);
    bson_append_bool(childP, kNodeP->name, -1, kNodeP->value.b);
    bson_append_document_end(container, childP);
    break;

  case KjFloat:
    bson_append_document_begin(container, kNodeP->name, -1, childP);
    bson_append_double(childP, kNodeP->name, -1, kNodeP->value.f);
    bson_append_document_end(container, childP);
    break;

  case KjNone:
    *detailsP = "Invalid type of Kjson field";
    return false;
  }

  if (kNodeP->type == KjObject)
  {
    for (KjNode* kChildP = kNodeP->value.firstChildP; kChildP != NULL; kChildP = kChildP->next)
    {
      printf("Object '%s' has a member '%s'\n", kNodeP->name, kChildP->name);
      memberP = (bson_t*) malloc(sizeof(bson_t));  // Create the BSON item (not knowing of what type it is
      if (memberP == NULL)
      {
        *detailsP = "out of memory";
        return false;
      }

      if (kjBson2(objectP, memberP, kChildP, detailsP) == false)
        return false;
    }
  }

  else if (kNodeP->type == KjArray)
  {
    for (KjNode* kChildP = kNodeP->value.firstChildP; kChildP != NULL; kChildP = kChildP->next)
    {
      memberP = (bson_t*) malloc(sizeof(bson_t));  // Create the BSON item (not knowing of what type it is
      if (memberP == NULL)
      {
        *detailsP = "out of memory";
        return false;
      }
      
      bson_append_array_begin(arrayP, kNodeP->name, -1, memberP);
      if (kjBson2(arrayP, memberP, kChildP, detailsP) == false)
        return false;
      bson_append_array_end(arrayP, memberP);
    }
  }

  return true;
}



// -----------------------------------------------------------------------------
//
// kjBson - work in progress ...
//
// If we make the call to bson_init as a prerequisite, we could avoid this function altogether
//
bool kjBson(bson_t* bsonTop, KjNode* kjsonTop, char** detailsP)
{
  bson_init(bsonTop);

  return kjBson2(NULL, bsonTop, kjsonTop, detailsP);
}
