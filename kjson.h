// 
// FILE            kjson.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJSON_H
#define KJSON_KJSON_H

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE

#include "kalloc/KAlloc.h"              // KAlloc
#include "kjson/KjStatus.h"             // KjStatus
#include "kjson/KjNode.h"               // KjNode



// -----------------------------------------------------------------------------
//
// Forward declaration of main struct of the library
//
struct Kjson;



// ----------------------------------------------------------------------------- 
//
// KjSaxEvent
//
typedef enum KjSaxEvent
{
  KjSaxStart,
  KjSaxEnd,
  KjSaxError,
  KjObjectStart,
  KjObjectEnd,
  KjArrayStart,
  KjArrayEnd,
  KjStringValue,
  KjFloatValue,
  KjIntegerValue,
  KjNullValue,
  KjBoolValue
} KjSaxEvent;



// ----------------------------------------------------------------------------- 
//
// KjSaxFunction - function type for SAX callbacks
//
typedef int (*KjSaxFunction)(struct Kjson* kjP, KjSaxEvent event, char* name, KjValue* valueP, KBool inArray);



// ----------------------------------------------------------------------------- 
//
// KjErrorFunction - function type for Error callbacks
//
typedef void (*KjErrorFunction)(struct Kjson* envP, int lineNo, int bufPos, char* errorString, char* restOfBuffer);



// -----------------------------------------------------------------------------
//
// KjAddFunction - function type for adding nodes to containers (object or array)
//
typedef void (*KjAddFunction)(KjNode* container, KjNode* nodeP);



// ----------------------------------------------------------------------------- 
//
// Kjson - 
//
typedef struct Kjson
{
  // input pointers
  char*           json;              // points to start of the input json buffer to parse
  char*           jsonP;             // points to current position of json input buffer 'Kjson::json'

  // Allocator
  KAlloc*         kallocP;           // points to the allocator

  // output
  KjNode*         tree;              // tree of kjson nodes as result of parse

  // Error handling
  int             lineNo;            // Contains the line number of the erroneous line
  int             errorPos;          // Contains the position in the buffer where the error was detected
  char            errorString[256];  // description of the latest error
  KBool           errorReported;     // To avoid reporting an error more than once (different levels in src code)

  // Callbacks
  KjSaxFunction   saxF;           // SAX callback during parse
  KjErrorFunction errorF;         // Error callback during parse - NOTE: kjson stops after first error
  KjAddFunction   addF;           // function pointer to 'add' function. Default: kjChildAdd
  //
  // add function
  //
  // The reason for this function is for kjson to be configurable to do sorted inserts
  // in containers (objects only - arrays are never sorted).
  // When using a function pointer, the pointer can just point to a different function
  // when configured to do sorted inserts (or reverse-sorte inserts)
  //

  // Common Configuration
  KBool           verbose;                      // verbose parsing - NOTE: output goes to stdout

  // Render Configuration
  char*           iStrings;                     // points to allocated area for the indents
  char**          iVec;                         // vector of indentation strings. Points inside iStrings
  KBool           indentLevelBufferReady;       // indentLevelBuffer is prepared, cannot be modified now
  int             maxIndentLevel;               // maximum number of levels for indentation
  int             spacesPerIndent;              // number of spaces per intentation
  char*           nlString;                     // newline string. Common values: "" and "\n"
  char*           stringBeforeColon;            // string before colon. Common values: "" and " "
  char*           stringAfterColon;             // string after colon: Common values: "" and " "
  KBool           objectStartBracketOnNewLine;  // render object start bracket on its own line
  KBool           arrayStartBracketOnNewLine;   // render array start bracket on its own line
  KBool           arraysOnOneLine;              // no newlines when rendering arrays
  KBool           shortArraysOnOneLine;         // no newlines when rendering 'short arrays'
  KBool           objectsOnOneLine;             // no newlines when rendering objects
  KBool           shortObjectsOnOneLine;        // no newlines when rendering 'short objects'
  KBool           numbersAsStrings;             // to avoid rounding errors when just beautifying
  KBool           spaceAfterComma;              // put a space after comma (only if no newline)
  int             shortArrayMaxLen;             // max render-len for an array to be considered 'short'
  int             shortObjectMaxLen;            // max render-len for an object to be considered 'short'
} Kjson;

#endif  // KJSON_KJSON_H
