//
// FILE            ktime.cpp - tput comparison with rapidjson
//
// AUTHOR          Ken Zangelin
//
// Copyright 2019 Ken Zangelin
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
//#include <bson.h>

#include "rapidjson/document.h"
using namespace rapidjson;

extern "C"
{
#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "klog/kLog.h"                  // K Log Lib
#include "kalloc/kaBufferInit.h"        // kaBufferInit

#include "kjson/kjson.h"                // kjson library
#include "kjson/kjParse.h"              // TBD
#include "kjson/kjRender.h"             // TBD
#include "kjson/kjConfig.h"             // kjConfig
#include "kjson/kjBufferCreate.h"       // kjBufferCreate
#include "kjson/kjFree.h"               // kjFree
#include "kjson/kjLookup.h"             // kjLookup
#include "kjson/kjClone.h"              // kjClone
//#include "kjson/kjBson.h"               // kjBson
}



/* ****************************************************************************
*
* MAX_MB -
*/
#define MAX_MB    64



/* ****************************************************************************
*
* progName -
*/
char* progName      = (char*) "ktime";
bool  parseNumbers  = false;



// -----------------------------------------------------------------------------
//
// logFunction -
//
void logFunction
(
  int          severity,              // 1: Error, 2: Warning, 3: Info, 4: Msg, 5: Verbose, 6: Trace
  int          level,                 // Trace level || Error code || Info Code
  const char*  fileName,
  int          lineNo,
  const char*  functionName,
  const char*  format,
  ...
)
{
}



/* ****************************************************************************
*
* clock_difftime -
*/
void clock_difftime(struct timespec* endTime, struct timespec* startTime, struct timespec* diffTime)
{
  diffTime->tv_nsec = endTime->tv_nsec - startTime->tv_nsec;
  diffTime->tv_sec  = endTime->tv_sec  - startTime->tv_sec;

  if (diffTime->tv_nsec < 0)
  {
    diffTime->tv_sec -= 1;
    diffTime->tv_nsec += 1000000000;
  }
}



/* ****************************************************************************
*
* usage -
*/
static void usage(void)
{
  printf("%s -f <json file> [-u (show usage and exit)] [-v (verbose)] [-t <times>] [-r (render JSON after parse)] [-pn (parse numbers)] [--timeOutput] [--lookup (name)]\n", progName);
}


char kallocBuffer[2 * 1024 * 1024];
// -----------------------------------------------------------------------------
//
// kjsonTest -
//
KjNode* kjsonTest(char* buf)
{
  Kjson   kj;
  Kjson*  kjP;
  KAlloc  kalloc;

  kaBufferInit(&kalloc, kallocBuffer, sizeof(kallocBuffer), 2 * 1024 * 1024, NULL, (char*) "KJSON Alloc Buffer");
  kjP = kjBufferCreate(&kj, &kalloc);

  return kjParse(kjP, buf);
}



// -----------------------------------------------------------------------------
//
// kjsonTestMulti -
//
KjNode* kjsonTestMulti(char** buf, int times)
{
  Kjson   kj;
  Kjson*  kjP;
  KAlloc  kalloc;
  int     ix  = 0;

#if 0
  if (kaInit() != KasOk)
    return NULL;
#endif

  kaBufferInit(&kalloc, kallocBuffer, sizeof(kallocBuffer), 16 * 1024, NULL, (char*) "KJSON Alloc Buffer");
  kjP = kjBufferCreate(&kj, &kalloc);

  KjNode* toplevel = NULL;

  while (times > 0)
  {
    --times;

    if (parseNumbers == true)
      kjConfig(kjP, KjcNumbersAsStrings, (char*) "No");
    else
      kjConfig(kjP, KjcNumbersAsStrings, (char*) "Yes");

    if ((toplevel = kjParse(kjP, buf[ix++])) == NULL)
      return NULL;

    kjFree(toplevel);
  }

  return toplevel;
}



// -----------------------------------------------------------------------------
//
// rapidjsonTest -
//
bool rapidjsonTest(char* buf)
{
  Document document;

  document.Parse(buf);

  if (document.HasParseError())
    return false;

  return true;
}



// -----------------------------------------------------------------------------
//
// rapidjsonParseForLookup
//
bool rapidjsonParseForLookup(Document& document, char* buf)
{
  document.Parse(buf);

  if (document.HasParseError())
    return false;

  return true;
}


// -----------------------------------------------------------------------------
//
// rapidjsonTest -
//
bool rapidjsonTestMulti(char** bufV, int times)
{
  int ix = 0;

  while (times > 0)
  {
    Document document;

    --times;

    document.Parse(bufV[ix]);

    if (document.HasParseError())
      return false;

    // document.delete()
    --ix;
  }

  return true;
}


#if 0
int mongoBsonFromJson(char* json)
{
  bson_error_t error;
  bson_t*      bson;

  bson = bson_new_from_json((const uint8_t*) json, -1, &error);

  if (!bson)
  {
    fprintf(stderr, "%s\n", error.message);
    return -1;
  }

  //  string = bson_as_json(bson, NULL) - This seems to be to get bask to json ...

  // bson_free (string);
  return 0;
}
#endif


/* ****************************************************************************
*
* main -
*/
int main(int argC, char* argV[])
{
  char*            jsonFile   = NULL;
  struct timespec  start;
  struct timespec  end;
  struct timespec  diff;
  int              render     = 0;
  int              times      = 1;
  KBool            verbose    = KFALSE;
  KBool            timeOutput = KFALSE;
  char*            lookup     = NULL;

  //
  // Parse args
  //
  int ix;
  for (ix = 1; ix < argC; ++ix)
  {
    if (strcmp(argV[ix], "-u") == 0)
    {
      usage();
      exit(1);
    }
    else if (strcmp(argV[ix], "-f") == 0)
    {
      jsonFile = argV[ix + 1];
      ++ix;
    }
    else if (strcmp(argV[ix], "-p") == 0)
    {
      render = 1;
    }
    else if (strcmp(argV[ix], "-v") == 0)
    {
      verbose = KTRUE;
    }
    else if (strcmp(argV[ix], "-pn") == 0)
    {
      parseNumbers = true;
    }
    else if (strcmp(argV[ix], "-t") == 0)
    {
      times = atoi(argV[ix + 1]);
      ++ix;
    }
    else if (strcmp(argV[ix], "--timeOutput") == 0)
    {
      timeOutput = KTRUE;
    }
    else if (strcmp(argV[ix], "--lookup") == 0)
    {
      lookup = argV[ix + 1];
      ++ix;
    }
    else
    {
      fprintf(stderr, "%s: unrecognized option/parameter: '%s'\n", progName, argV[ix]);
      usage();
      exit(1);
    }
  }

  if (jsonFile == NULL)
  {
    fprintf(stderr, "%s: no json file to parse\n", progName);
    usage();
    exit(1);
  }

  //
  // Read file content into buffer
  //
  struct stat s;
  if (stat(jsonFile, &s) != 0)
  {
    fprintf(stderr, "%s: stat(%s): %s\n", progName, jsonFile, strerror(errno));
    exit(2);
  }

  if (s.st_size > MAX_MB * 1024 * 1024)
  {
    fprintf(stderr, "%s: json file '%s' too big (max is %dMb)\n", progName, jsonFile, MAX_MB);
    exit(2);
  }


  int   nb;
  int   sz = s.st_size;
  int   fd;
  char* buf1 = (char*) calloc(1, s.st_size + 1);
  char* buf2;
  char* buf3;

  if (buf1 == NULL)
  {
    fprintf(stderr, "%s: allocating buffer for file contents: %s\n", progName, strerror(errno));
    exit(3);
  }

  if ((fd = open(jsonFile, O_RDONLY)) == -1)
  {
    fprintf(stderr, "%s: open(%s): %s\n", progName, jsonFile, strerror(errno));
    exit(4);
  }

  if ((nb = read(fd, buf1, sz)) != sz)
  {
    fprintf(stderr, "%s: reading from '%s': %s\n", progName, jsonFile, strerror(errno));
    exit(5);
  }

  if ((buf2 = strdup(buf1)) == NULL)
  {
    fprintf(stderr, "%s: duplicating contents of '%s': %s\n", progName, jsonFile, strerror(errno));
    exit(6);
  }

  if ((buf3 = strdup(buf1)) == NULL)
  {
    fprintf(stderr, "%s: duplicating contents of '%s': %s\n", progName, jsonFile, strerror(errno));
    exit(6);
  }

  long long  t1;
  long long  t2;
  char**     buf1V = NULL;
  char**     buf2V = NULL;
  char**     buf3V = NULL;
  KjNode*    kjNodeP;

  if (times > 1)
  {
    buf1V = (char**) malloc(times * sizeof(char*));
    buf2V = (char**) malloc(times * sizeof(char*));
    buf3V = (char**) malloc(times * sizeof(char*));

    int ix;
    for (ix = 0; ix < times; ix++)
    {
      buf1V[ix] = strdup(buf1);
      buf2V[ix] = strdup(buf1);
      buf3V[ix] = strdup(buf1);
    }
  }

  //
  // mongo bson_from_json
  //
#if 0
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
  if (mongoBsonFromJson(buf3) == -1)
  {
    printf("mongoBsonFromJson failed");
    exit(1);
  }

  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
  clock_difftime(&end, &start, &diff);

  printf("mongo bson_from_json: %lu microseconds\n", diff.tv_nsec / 1000);
#endif

  //
  // kjson
  //
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
  if (times == 1)
    kjNodeP = kjsonTest(buf1);
  else
    kjNodeP = kjsonTestMulti(buf1V, times);

  if (kjNodeP == NULL)
  {
    printf("kjson Parse Error\n");
    return 1;
  }
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
  clock_difftime(&end, &start, &diff);
  t1 = diff.tv_sec * 1000000000 + diff.tv_nsec;

  if (verbose == KTRUE)
  {
    printf("kjson:     %ld.%09ld\n", diff.tv_sec, diff.tv_nsec);
    printf("kjson:     %lld\n", t1);
  }

  //
  // Testing kjBson
  //
#if 0
  bson_t bsonTop;
  bool   b;
  char*  details;

  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
  b = kjBson(&bsonTop, kjNodeP, &details);
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
  clock_difftime(&end, &start, &diff);

  if (b == false)
  {
    printf("kjson BSON conversion error\n");
    return 1;
  }

  printf("kjBson: %ld.%09ld\n", diff.tv_sec, diff.tv_nsec);
#endif

  //
  // rapidjson
  //
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
  bool ok;
  if (times == 1)
  {
    ok = rapidjsonTest(buf2);
    if (!ok)
    {
      printf("rapidjson Parse Error\n");
      return 2;
    }
  }
  else  // rapidjsonTestMulti crashes ...
  {
    for (int ix = 0; ix < times; ix++)
    {
      ok = rapidjsonTest(buf2V[ix]);
      if (!ok)
      {
        printf("rapidjson Parse Error\n");
        return 2;
      }
    }
  }
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
  clock_difftime(&end, &start, &diff);
  t2 = diff.tv_sec * 1000000000 + diff.tv_nsec;

  if (verbose == KTRUE)
  {
    printf("rapidjson: %ld.%09ld\n", diff.tv_sec, diff.tv_nsec);
    printf("rapidjson: %lld\n", t2);
  }


  //
  // Compare and show result
  //
  double rate;
  double rate2;

  if (t1 < t2)
  {
    rate  = (double) t2 / t1 - 1;
    rate2 = (double) t2 / t1;
  }
  else
  {
    rate = (double) t1 / t2 - 1;
  }

  if (timeOutput)
  {
    printf("kjson:%llu,rapidjson:%llu\n", t1, t2);
    exit(0);
  }
  else if (t1 < t2)
  {
    printf("kjson (rate: %.2f) is %.2f%% faster than rapidjson %s for '%s' (kjson: %.2f us - rapidjson: %.2f us)\n", rate2, rate * 100, RAPIDJSON_VERSION_STRING, jsonFile, t1/1000.0, t2/1000.0);
  }
  else
    printf("!!!!! rapidjson %s is %.2f%% faster than kjson for '%s' (kjson: %.2f us - rapidjson: %.2f us)\n", RAPIDJSON_VERSION_STRING, rate * 100, jsonFile, t1/1000.0, t2/1000.0);


  char   rBuf[1024 * 8];
  KAlloc kalloc;
  Kjson  kj;
  Kjson* kjP;

  kaBufferInit(&kalloc, kallocBuffer, sizeof(kallocBuffer), 16 * 1024, NULL, (char*) "KJSON Alloc Buffer");
  kjP = kjBufferCreate(&kj, &kalloc);

  if (render)
  {
    kjRender(kjP, kjNodeP, rBuf, sizeof(rBuf));
    printf("%s\n", rBuf);
  }

  if (verbose == KTRUE)
    printf("-----------------------------------------\n");


  if (lookup != NULL)
  {
    Document document;
    KjNode*  found;
    char*    bufCopy = strdup(buf2);

    // Prepare rapidjson tree
    rapidjsonParseForLookup(document, buf2);

    // Prepare KjNode tree
    kjNodeP = kjParse(kjP, bufCopy);

    // Normal KJSON lookup
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);

    found = kjLookup(kjNodeP, lookup);

    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
    clock_difftime(&end, &start, &diff);

    if (found == NULL)
    {
      printf("Normal KJSON lookup: '%s' not found ...\n", lookup);
      exit(1);
    }

    printf("KJSON kStrEq lookup of '%s' took        %lu nanosecs\n", lookup, diff.tv_nsec);


    // KJSON strcmp lookup
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);

    found = kjLookupWithStrcmp(kjNodeP, lookup);

    if (found == NULL)
    {
      printf("KJSON strcmp lookup: '%s' not found ...\n", lookup);
      exit(1);
    }

    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
    clock_difftime(&end, &start, &diff);

    printf("KJSON strcmp lookup of '%s' took        %lu nanosecs\n", lookup, diff.tv_nsec);


    // KJSON char-sum lookup
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);

    found = kjLookupWithCharSum(kjNodeP, lookup);

    if (found == NULL)
    {
      printf("KJSON char-sum lookup: '%s' not found ...\n", lookup);
      exit(1);
    }

    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
    clock_difftime(&end, &start, &diff);

    printf("KJSON char-sum lookup of '%s' took        %lu nanosecs\n", lookup, diff.tv_nsec);


    // RAPIDJSON lookup
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);

    Value::ConstMemberIterator found2 = document.FindMember(lookup);

    if (found2 == document.MemberEnd())
    {
      printf("rapidjson C++ ==: '%s' not found ...\n", lookup);
      exit(1);
    }
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
    clock_difftime(&end, &start, &diff);

    printf("RAPIDJSON lookup of '%s' took      %lu nanosecs\n", lookup, diff.tv_nsec);
  }

  return 0;
}
