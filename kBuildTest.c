// 
// FILE            kBuildTest.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // printf
#include <string.h>                     // strcmp
#include <stdlib.h>                     // exit

#include "kbase/kBasicLog.h"            // KBL_*
#include "kalloc/kaBufferInit.h"        // kaBufferInit

#include "kjson/kjConfig.h"             // kjConfig
#include "kjson/kjBuilder.h"            // json builder
#include "kjson/kjRender.h"             // kjRender
#include "kjson/kjBufferCreate.h"       // kjBufferCreate



// -----------------------------------------------------------------------------
//
// progName -
//
char* progName = "kBuildTest";



// -----------------------------------------------------------------------------
//
// usage - 
//
static void usage(void)
{
  char* empty = strdup(progName);
  char* eP    = empty;
  
  while (*eP)
  {
    *eP = ' ';
    ++eP;
  }

  printf("%s [-u (show this text)]\n"
         "%s [-i (spaces per indent)\n"
         "%s [-min (no whitespace at all)\n", progName, empty, empty);

  exit(1);
}



char kallocBuffer[128 * 1024];
// -----------------------------------------------------------------------------
//
// main - 
//
int main(int argC, char* argV[])
{
  char*  newlineString  = NULL;
  char*  indentStep     = NULL;
  int    minimized      = 0;
  Kjson  kj;
  Kjson* kjP;
  KAlloc kalloc;

  //
  // Parsing command line args
  //
  int ix = 1;
  while (ix < argC)
  {
    if (strcmp(argV[ix], "-u") == 0)
      usage();
    else if (strcmp(argV[ix], "-i") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing integer after -i\n", progName);
        usage();
      }
      indentStep = argV[ix];
    }
    else if (strcmp(argV[ix], "-min") == 0)
      minimized = 1;

    ++ix;
  }
  
  kaBufferInit(&kalloc, kallocBuffer, sizeof(kallocBuffer), 16 * 1024, NULL, (char*) "KJSON Alloc Buffer");
  kjP = kjBufferCreate(&kj, &kalloc);
    
  //
  // Configure kjson library
  //
  if (newlineString != NULL)
    kjConfig(kjP, KjcNewlineString, newlineString);

  if (indentStep != NULL)
    kjConfig(kjP, KjcIndentStep, indentStep);

  if (minimized == 1)
    kjConfig(kjP, KjcMinimized, NULL);



  //
  // Creating JSON tree
  //
  KjNode* top = kjObject(kjP, "top");
  KjNode* a   = kjArray(kjP, "a");
  KjNode* o1  = kjString(kjP, "s", "a string");
  KjNode* i1  = kjInteger(kjP, "fourteen", 14);
  
  kjChildAdd(top, a);
  kjChildAdd(a, i1);
  kjChildAdd(a, o1);



  //
  // Rendering buffer
  //
  char rBuf[1024];

  kjRender(kjP, top, rBuf, sizeof(rBuf));
  printf("%s\n", rBuf);

  return 0;
}
