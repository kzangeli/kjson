// 
// FILE            kjReset.c - reset a kjson buffer
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stddef.h>                     // NULL

#include "kjson/kjson.h"                // main header file of the library
#include "kjson/KjStatus.h"             // KjStatus
#include "kjson/kjDefaults.h"           // KJ_DEFAULT_MAX_INDENT_LEVEL, et al
#include "kjson/kjReset.h"              // Own interface



// -----------------------------------------------------------------------------
//
// kjReset - reset the KJson buffer, to get ready for a new parse
//
KjStatus kjReset(Kjson* kjP)
{
  kjP->json                         = NULL;
  kjP->jsonP                        = NULL;

  kjP->tree                         = NULL;

  kjP->lineNo                       = 1;
  kjP->errorString[0]               = 0;
  kjP->errorReported                = KFALSE;
 
  kjP->saxF                         = NULL;
  kjP->errorF                       = NULL;
  kjP->addF                         = NULL;

  //
  // Configuration
  //
  kjP->iStrings                     = NULL;
  kjP->iVec                         = NULL;
  kjP->indentLevelBufferReady       = KFALSE;
  kjP->maxIndentLevel               = KJ_DEFAULT_MAX_INDENT_LEVEL;
  kjP->spacesPerIndent              = KJ_DEFAULT_SPACES_PER_INDENT;
  kjP->nlString                     = KJ_DEFAULT_NL_STRING;
  kjP->stringBeforeColon            = KJ_DEFAULT_STRING_BEFORE_COLON;
  kjP->stringAfterColon             = KJ_DEFAULT_STRING_AFTER_COLON;
  kjP->objectStartBracketOnNewLine  = KJ_DEFAULT_OBJECT_START_BRACKET_ON_NEW_LINE;
  kjP->arrayStartBracketOnNewLine   = KJ_DEFAULT_ARRAY_START_BRACKET_ON_NEW_LINE;
  kjP->numbersAsStrings             = KJ_DEFAULT_NUMBERS_AS_STRINGS;
  kjP->spaceAfterComma              = KFALSE;  // Only used temporarily in short arrays/objects on single line
  kjP->shortArrayMaxLen             = KJ_DEFAULT_SHORT_ARRAY_MAX_LEN;
  kjP->shortObjectMaxLen            = KJ_DEFAULT_SHORT_OBJECT_MAX_LEN;

  return KjsOk;
}
