// 
// FILE            kjConfig.c - configure the behaviour of kjson
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdlib.h>                     // atoi
#include <string.h>                     // strlen
#include <stdio.h>                      // sprintf

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "kbase/kLibLog.h"              // K Log macros

#include "kjson/kjson.h"                // Kjson struct
#include "kjson/KjStatus.h"             // KjStatus
#include "kjson/kjBuilder.h"            // kjChildAddSorted
#include "kjson/kjDefaults.h"           // Default values
#include "kjson/kjConfig.h"             // Own Interface



// -----------------------------------------------------------------------------
//
// kjConfig
//
KjStatus kjConfig(Kjson* kjP, KjConfigItem item, char* value)
{
  switch (item)
  {
  case KjcSaxFunction:
#ifdef KJ_SAX_ON
    kjP->saxF = (KjSaxFunction) value;
#else
    kjP->saxF = (KjSaxFunction) NULL;
    kjErrorStringSet(kjP, "Sax functionality is not included in this build. Please reconfigure kjson and rebuild");
    return KjsBadParam;
#endif
    break;

  case KjcErrorFunction:
    kjP->errorF = (KjErrorFunction) value;
    break;

  case KjcIndentStep:
    if ((value == NULL) || (*value == 0))
      return KjsBadParam;

    if (kjP->indentLevelBufferReady == KTRUE)
      return KjsAlreadyConfigured;

    kjP->spacesPerIndent = atoi(value);
    // indentLevelInit(kjP) is called by kjRender
    break;

  case KjcNewlineString:
    if (value == NULL)
      kjP->nlString = "";
    else if (*value == 0)
      kjP->nlString = "";
    else if ((*value == '\n') && (value[1] == 0))
      kjP->nlString = "\n";
    else if ((*value == ' ') && (value[1] == 0))
      kjP->nlString = " ";
    else
    {
      kjP->nlString = malloc(strlen(value) + 1);
      sprintf(kjP->nlString, "%s", value);
      kjP->nlString[strlen(value)] = 0;
    }
    break;

  case KjcDefault:
    kjP->nlString                    = "\n";
    kjP->spacesPerIndent             = 2;
    kjP->stringBeforeColon           = "";
    kjP->stringAfterColon            = " ";
    kjP->objectStartBracketOnNewLine = KFALSE;
    kjP->arrayStartBracketOnNewLine  = KFALSE;
    break;

  case KjcSpaceBeforeColon:
    kjP->stringBeforeColon = " ";
    break;

  case KjcNoSpaceAfterColon:
    kjP->stringAfterColon = "";
    break;

  case KjcObjectStartOnNewLine:
    kjP->objectStartBracketOnNewLine = KTRUE;
    break;

  case KjcArrayStartOnNewLine:
    kjP->arrayStartBracketOnNewLine = KTRUE;
    break;

  case KjcArraysOnOneLine:
    kjP->arraysOnOneLine = KTRUE;
    break;

  case KjcShortArraysOnOneLine:
    kjP->shortArraysOnOneLine = KTRUE;
    break;

  case KjcObjectsOnOneLine:
    kjP->objectsOnOneLine = KTRUE;
    break;

  case KjcShortObjectsOnOneLine:
    kjP->shortObjectsOnOneLine = KTRUE;
    break;

  case KjcMinimized:
    kjP->nlString                    = "";
    kjP->spacesPerIndent             = 0;
    kjP->stringBeforeColon           = "";
    kjP->stringAfterColon            = "";
    kjP->objectStartBracketOnNewLine = KFALSE;
    kjP->arrayStartBracketOnNewLine  = KFALSE;
    break;

  case KjcNumbersAsStrings:
    if      (value  == NULL)   kjP->numbersAsStrings = KFALSE;
    else if (*value == 0)      kjP->numbersAsStrings = KFALSE;
    else if (*value == 'Y')    kjP->numbersAsStrings = KTRUE;
    else                       kjP->numbersAsStrings = KFALSE;
    break;

  case KjcUnsorted:
    kjP->addF = NULL;
    break;

  case KjcSorted:
    kjP->addF = kjChildAddSorted;
    break;

  case KjcSortedReverse:
    kjP->addF = kjChildAddSortedReverse;
    break;

  case KjcShortArrayMaxLen:
    if (value == NULL)
      kjP->shortArrayMaxLen = KJ_DEFAULT_SHORT_ARRAY_MAX_LEN;
    else if (*value == 0)
      kjP->shortArrayMaxLen = KJ_DEFAULT_SHORT_ARRAY_MAX_LEN;
    else
    {
      kjP->shortArrayMaxLen = atoi(value);

      if (kjP->shortArrayMaxLen < 3)
      {
        kjP->shortArrayMaxLen = KJ_DEFAULT_SHORT_ARRAY_MAX_LEN;
      }
      else
        return KjsOk;
    }

    // Error Callback: invalid value for KjcShortArrayMaxLen
    break;

  case KjcShortObjectMaxLen:
    if (value == NULL)
      kjP->shortObjectMaxLen = KJ_DEFAULT_SHORT_OBJECT_MAX_LEN;
    else if (*value == 0)
      kjP->shortObjectMaxLen = KJ_DEFAULT_SHORT_OBJECT_MAX_LEN;
    else
    {
      kjP->shortObjectMaxLen = atoi(value);
      if (kjP->shortObjectMaxLen < 3)
      {
        kjP->shortObjectMaxLen = KJ_DEFAULT_SHORT_OBJECT_MAX_LEN;
      }
      else
        return KjsOk;
    }
    
    // Error Callback: invalid value for KjcShortObjectMaxLen
    break;
  }

  return KjsOk;
}
