// 
// FILE            KjStatus.c - utility function for 'enum kjStatus'
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kjson/KjStatus.h"    // Own Interface



// -----------------------------------------------------------------------------
//
// kjStatus - 
//
const char* kjStatus(KjStatus s)
{
  switch (s)
  {
  case KjsOk:                          return "OK";
  case KjsBadParam:                    return "Bad Parameter";
  case KjsAllocError:                  return "Alloc Error";
  case KjsParseError:                  return "Parse Error";
  case KjsBufSizeTooSmall:             return "Buf Size Too Small";
  case KjsAlreadyConfigured:           return "Already Configured";
  case KjsAlreadyInitialized:          return "Already Initialized";
  }

  return "Unknown Error";
}
