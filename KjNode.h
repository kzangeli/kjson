// 
// FILE            KjNode.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
//
// This file defines the KjNode structure, that contains everything necessary for
// the nodes in the JSON tree that is created as output of the parse step.
//
#ifndef KJSON_KJNODE_H_
#define KJSON_KJNODE_H_

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE



// -----------------------------------------------------------------------------
//
// KjValueType - 
//
typedef enum KjValueType
{
  KjNone,
  KjString,
  KjInt,
  KjFloat,
  KjBoolean,
  KjNull,
  KjObject,
  KjArray
} KjValueType;



struct KjNode;
// -----------------------------------------------------------------------------
//
// KjValue - value of a node in the JSON tree
//
typedef union KjValue
{
  KBool           b;
  long long       i;
  double          f;
  char*           s;
  struct KjNode*  firstChildP;
} KjValue;



// -----------------------------------------------------------------------------
//
// KjNode - struct describing a node in a JSON tree
//
typedef struct KjNode
{
  char*           name;        // The name of the node, "" if father is an Array
  KjValueType     type;        // The type of the node. Number, String, Object, Array, ...
  KjValue         value;       // The value of the node - see KjValue
  struct KjNode*  next;        // Pointer to the next Sibling
  struct KjNode*  lastChild;   // Pointer to the last child of this node - FIXME: to be removed (move to struct KjContainer?)

#ifdef USE_CHAR_SUM
  unsigned short  cSum;        // char-sum over 'name'
#endif

#ifdef USE_VALUE_STRING
  char*           valueString; // The value of the node, as a string
#endif
} KjNode;



// -----------------------------------------------------------------------------
//
// kjValueType - 
//
extern const char* kjValueType(KjValueType vt);



// -----------------------------------------------------------------------------
//
// kjValue - 
//
extern char* kjValue(KjNode* nodeP, char* buf, int bufLen);

#endif  // KJSON_KJNODE_H_
