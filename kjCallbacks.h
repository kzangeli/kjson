// 
// FILE            kjCallbacks.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJ_CALLBACKS_H_
#define KJSON_KJ_CALLBACKS_H_

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE

#include "kjson/kjConfig.h"             // KJ_SAX_ON
#include "kjson/kjson.h"                // Kjson struct



// -----------------------------------------------------------------------------
//
// SAX - sax callbacks
//
#ifdef KJ_SAX_ON

#define KJ_SAX(kjP, event, name, valueP, inArray) \
do                                                \
{                                                 \
  if (kjP->saxF != NULL)                          \
  {                                               \
    kjP->saxF(kjP, event, name, valueP, inArray); \
  }                                               \
} while (0)

#define IN_ARRAY           ,inArray
#define IN_ARRAY_FALSE     ,KFALSE
#define IN_ARRAY_TRUE      ,KTRUE
#define IN_ARRAY_AS_PARAM  ,KBool inArray
#else
#define KJ_SAX(kjP, event, name, valueP, inArray)
#define IN_ARRAY
#define IN_ARRAY_AS_PARAM
#define IN_ARRAY_FALSE
#define IN_ARRAY_TRUE
#endif



// -----------------------------------------------------------------------------
//
// KJ_ERR -
//
#define KJ_ERR(kjP, offset)                                                       \
do                                                                                \
{                                                                                 \
  if (kjP->errorReported == KFALSE)                                               \
  {                                                                               \
    kjP->errorPos = (long long) kjP->jsonP - (long long) kjP->json + offset;      \
    KJ_SAX(kjP, KjSaxError, NULL, NULL, KFALSE);                                  \
                                                                                  \
    if (kjP->errorF != NULL)                                                      \
      kjP->errorF(kjP, kjP->lineNo, kjP->errorPos, kjP->errorString, kjP->jsonP); \
    kjP->errorReported = KTRUE;                                                   \
  }                                                                               \
} while (0)

#endif  // KJSON_KJ_CALLBACKS_H_
