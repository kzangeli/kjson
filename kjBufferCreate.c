// 
// FILE            kjBufferCreate.c - init routine for the kjson library
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kLibLog.h"              // K Log macros
#include "kalloc/KAlloc.h"              // KAlloc
#include "kalloc/kaBufferInit.h"        // kaBufferInit

#include "kjson/kjson.h"                // KJson
#include "kjson/KjStatus.h"             // KjStatus
#include "kjson/kjCallbacks.h"          // KJ_ERR
#include "kjson/KjTraceLevels.h"        // Trace Levels for the kjson library
#include "kjson/kjDefaults.h"           // KJ_DEFAULT_MAX_INDENT_LEVEL, et al
#include "kjson/kjBufferCreate.h"       // Own interface



// -----------------------------------------------------------------------------
//
// kjBufferCreate -
//
// The idea is for each thread to have its own Kjson buffer.
//
Kjson* kjBufferCreate(Kjson* kjP, KAlloc* kaP)
{
  if (kjP == NULL)
  {
    kjP = (Kjson*) calloc(1, sizeof(Kjson));

    if (kjP == NULL)
      return NULL;
  }
  else
    bzero(kjP, sizeof(Kjson));

  if (kaP == NULL)
  {
    char* buf = malloc(sizeof(KAlloc) + 1024 * 8);

    kaP = (KAlloc*) buf;
    kaBufferInit(kaP, &buf[sizeof(KAlloc)], 1024 * 8, 2048, NULL, "JSON Alloc Buffer");
  }

  kjP->kallocP = kaP;  // Its inital buffer should already be in place (in case pre-allocated)

  //
  // Default rendering settings:
  // FIXME: exact same thing done in kjReset()
  //
  kjP->iStrings                     = NULL;
  kjP->iVec                         = NULL;
  kjP->indentLevelBufferReady       = KFALSE;
  kjP->maxIndentLevel               = KJ_DEFAULT_MAX_INDENT_LEVEL;
  kjP->spacesPerIndent              = KJ_DEFAULT_SPACES_PER_INDENT;
  kjP->nlString                     = KJ_DEFAULT_NL_STRING;
  kjP->stringBeforeColon            = KJ_DEFAULT_STRING_BEFORE_COLON;
  kjP->stringAfterColon             = KJ_DEFAULT_STRING_AFTER_COLON;
  kjP->objectStartBracketOnNewLine  = KJ_DEFAULT_OBJECT_START_BRACKET_ON_NEW_LINE;
  kjP->arrayStartBracketOnNewLine   = KJ_DEFAULT_ARRAY_START_BRACKET_ON_NEW_LINE;
  kjP->numbersAsStrings             = KJ_DEFAULT_NUMBERS_AS_STRINGS;
  kjP->spaceAfterComma              = KFALSE;  // Only used temporarily in short arrays/objects on single line
  kjP->shortArrayMaxLen             = KJ_DEFAULT_SHORT_ARRAY_MAX_LEN;
  kjP->shortObjectMaxLen            = KJ_DEFAULT_SHORT_OBJECT_MAX_LEN;

  return kjP;
}
