// 
// FILE            KjNode.c - utility functions for node values
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // sprintf

#include "kbase/kMacros.h"              // K_FT, et al
#include "kbase/kLibLog.h"              // K Log macros

#include "kjson/KjNode.h"               // Own interface



// -----------------------------------------------------------------------------
//
// kjValueType - 
//
const char* kjValueType(KjValueType vt)
{
  switch (vt)
  {
  case KjNone:     return "None";
  case KjString:   return "String";
  case KjInt:      return "Int";
  case KjFloat:    return "Float";
  case KjBoolean:  return "Bool";
  case KjNull:     return "Null";
  case KjObject:   return "Object";
  case KjArray:    return "Array";
  }

  return "Unknown";
}



// -----------------------------------------------------------------------------
//
// kjValue - 
//
char* kjValue(KjNode* nodeP, char* buf, int bufLen)
{
  switch (nodeP->type)
  {
  case KjNone:     return "None";
  case KjString:   return nodeP->value.s;
  case KjObject:   return "Object";
  case KjArray:    return "Array";
  case KjBoolean:  return K_FT(nodeP->value.b == KTRUE);
  case KjNull:     return "Null";

  case KjInt:
#ifdef USE_VALUE_STRING
    if (nodeP->valueString != NULL)
      snprintf(buf, bufLen, "%s", nodeP->valueString);
    else
#endif
      snprintf(buf, bufLen, "%lld", nodeP->value.i);

    return buf;

  case KjFloat:
#ifdef USE_VALUE_STRING
    if (nodeP->valueString != NULL)
      snprintf(buf, bufLen, "%s", nodeP->valueString);
    else
#endif
      snprintf(buf, bufLen, "%f", nodeP->value.f);
    return buf;
  }

  return "unknown value";
}
