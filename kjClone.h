// 
// FILE            kjClone.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJCLONE_H_
#define KJSON_KJCLONE_H_

#include "kjson/kjson.h"                // Kjson
#include "kjson/KjNode.h"               // KjNode



// -----------------------------------------------------------------------------
//
// kjClone - clone a node
//
extern KjNode* kjClone(Kjson* kjP, KjNode* nodeP);

#endif  // KJSON_KJCLONE_H_
