//
// FILE            kjRender.c - stringify a JSON tree
//
// AUTHOR          Ken Zangelin
//
// Copyright 2019 Ken Zangelin
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdio.h>                      // printf
#include <string.h>                     // memset

#include "kbase/kMacros.h"              // K_FT, et al
#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "kbase/kFloatTrim.h"           // kFloatTrim
#include "kbase/kLibLog.h"              // K Log macros

#include "kalloc/kaAlloc.h"             // kaAlloc
#include "kjson/KjTraceLevels.h"        // Trace Levels for the kjson library
#include "kjson/kjson.h"                // Kjson struct
#include "kjson/kjRender.h"             // Own Interface



// -----------------------------------------------------------------------------
//
// KJ_INDENT_MAX_LEVEL -
//
#ifndef KJ_INDENT_MAX_LEVEL
#define KJ_INDENT_MAX_LEVEL 1024
#endif



// -----------------------------------------------------------------------------
//
// RenderBuffer -
//
typedef struct RenderBuffer
{
  char* buf;
  int   bytesUsed;
  int   bytesLeft;
} RenderBuffer;



// -----------------------------------------------------------------------------
//
// indentLevelInit - prepare char-pointers for indentation levels
//
// Setup kjP->iVec pointing inside kjP->iStrings.
//
// kjP->iStrings - buffer for all the indentation strings
//
// This buffer harbors the actual strings for the different indentation levels.
// `kjP->iVec` is a vector of char-pointers that point into `kjP->iStrings`.
// Once the number of spaces per indentation level (spacesPerIndent) is known,
// which is in runtime, this buffer can be prepared with spaces and zeroes (string termination)
// and `kjP->iVec` is set to point to the different strings, like this:
// [ spacesPerIndent == 2 ]
//
// - Fill entire kjP->iStrings with ' ' (0x20)
// - kjP->iVec[0] = "" (empty string)
// - kjP->iVec[1] = &kjP->iStrings[0]
// - kjP->iStrings[2] = 0;  // to terminate the first string
// - kjP->iVec[2] = &kjP->iStrings[3];
// - kjP->iStrings[7] = 0;  // to terminate the second string
// - etc.
// This in done in the function indentLevelInit.
//
static void indentLevelInit(Kjson* kjP)
{
  //
  // If no spaces wanted, kjP->iVec is not in use.
  //
  if (kjP->spacesPerIndent == 0)
  {
    return;
  }

  //
  // Just in case ...
  //
  if (kjP->indentLevelBufferReady == KTRUE)
  {
    return;
  }


  //
  // First thing to do is to decide size of buffer according to:
  //   - spacesPerIndent
  //   - maxIndentLevel
  //
  // We need a vector of maxIndentLevel char* (== 8 * maxIndentLevel)
  // And, we need ONE string per Indent Level, the first one of the length
  // spacesPerIndent + 1, next one with the length spacesPerIndent*2+1, etc
  //
  int bytesForIndentVector  = kjP->maxIndentLevel * sizeof(char*);
  int bytesForIndentStrings = 0;
  int ilev                  = 1;

  while (ilev < kjP->maxIndentLevel)
  {
    // printf("Level %d: bytes: %d\n", ilev, kjP->spacesPerIndent * ilev + 1);
    bytesForIndentStrings += kjP->spacesPerIndent * ilev + 1;
    ++ilev;
  }

  // Now we can allocate
  kjP->iVec     = (char**) kaAlloc(kjP->kallocP, bytesForIndentVector);
  kjP->iStrings = (char*)  kaAlloc(kjP->kallocP, bytesForIndentStrings);

  int ix;
  int bufIx = 0;

  kjP->iVec[0] = "";

  memset(kjP->iStrings, 0x20202020, bytesForIndentStrings);

  for (ix = 1; ix < kjP->maxIndentLevel; ix++)
  {
    int diff = ix * kjP->spacesPerIndent;

    // point to beginning
    kjP->iVec[ix] = &kjP->iStrings[bufIx];

    // step to end
    bufIx += diff;

    // Zero terminate
    kjP->iStrings[bufIx] = 0;
    // printf("Zero terminating for ilevel %d at pos %d (%d bytes)\n", ix, bufIx, diff);

    // Step over zero to the beginning of the next
    ++bufIx;
  }

  //  for (ix = 1; ix < kjP->maxIndentLevel; ix++)
  //    printf("Level %d: '%s'\n", ix, kjP->iVec[ix]);

  kjP->indentLevelBufferReady = KTRUE;
}



// -----------------------------------------------------------------------------
//
// indentGet - return indentation string according to indentation level
//
static char* indentGet(Kjson* kjP, int iLevel)
{
  if (kjP->spacesPerIndent == 0)
    return "";

  if (iLevel < kjP->maxIndentLevel)
    return kjP->iVec[iLevel];

  return kjP->iVec[kjP->maxIndentLevel - 1];
}



// -----------------------------------------------------------------------------
//
// pushChar -
//
#define pushChar(bP, c)                          \
do {                                             \
  if (bP->bytesLeft > 0)                         \
  {                                              \
    bP->buf[bP->bytesUsed] = c;                  \
    ++bP->bytesUsed;                             \
    --bP->bytesLeft;                             \
  }                                              \
} while (0)



// -----------------------------------------------------------------------------
//
// pushString -
//
#define pushString(bP, s)                        \
do {                                             \
  char* ss = (s == NULL)? "" : s;                \
  while ((bP->bytesLeft > 0) && (*ss != 0))      \
  {                                              \
    bP->buf[bP->bytesUsed] = *ss;                \
    ++bP->bytesUsed;                             \
    --bP->bytesLeft;                             \
    ++ss;                                        \
  }                                              \
} while (0)



// -----------------------------------------------------------------------------
//
// pushInt -
//
#define pushInt(bP, i)                           \
do {                                             \
  char number[32];                               \
  snprintf(number, sizeof(number), "%lld", i);   \
  pushString(bP, number);                        \
} while (0)



// -----------------------------------------------------------------------------
//
// pushFloat -
//
#define pushFloat(bP, f)                         \
do {                                             \
  char number[32];                               \
  snprintf(number, sizeof(number), "%f", f);     \
  kFloatTrim(number);                            \
  pushString(bP, number);                        \
} while (0)



// -----------------------------------------------------------------------------
//
// pushBool -
//
#define pushBool(bP, b)                                                  \
do {                                                                     \
  char cBuf[32];                                                         \
  snprintf(cBuf, sizeof(cBuf), "%s", K_FT(b));                           \
  pushString(bP, cBuf);                                                  \
} while (0)



// -----------------------------------------------------------------------------
//
// renderSize2 - size of rendered object
//
// This pair of functions is used only to see the size of an array/object to
// decide whether to put it on a sinlge line or not.
// If the array/object contains another nested array/object, we don't want it on a
// single line, so an absurdly high number for size is returned
//
static int renderSize2(KjNode* nodeP, KBool inVec)
{
  int extraForObject = (inVec == KFALSE)? strlen(nodeP->name) + 2 + 1 : 0;   // two double-quotes and a colon: 2+1
  int extraForComma  = (nodeP->next != NULL)? 1 : 0;
  int extra          = extraForObject + extraForComma;

  if (nodeP->type == KjInt)
  {
    char cv[64];

    snprintf(cv, sizeof(cv), "%lld", nodeP->value.i);
    return extra + strlen(cv);
  }
  else if (nodeP->type == KjFloat)
  {
    char cv[64];

    snprintf(cv, sizeof(cv), "%f", nodeP->value.f);
    return extra + strlen(cv);
  }
  else if (nodeP->type == KjString)
    return extra + strlen(nodeP->value.s);
  else if (nodeP->type == KjBoolean)
  {
    if (nodeP->value.b == KTRUE)
      return extra + 4;

    return extra + 5;
  }
  else if (nodeP->type == KjNull)
    return extra + 4;

  // Array or Object
  return 10000;
}



// -----------------------------------------------------------------------------
//
// renderSize - size of rendered object
//
// NOTE
//   The nodeP must be either a KjObject or an KjArray.
//   The code guarantees this, no check needed.
//
static int renderSize(KjNode* nodeP, KBool inVec)
{
  KBool isArray = (nodeP->type == KjArray)? KTRUE : KFALSE;

  KjNode*  nP        = nodeP->value.firstChildP;
  int      totalSize = 2;  // Start and end brackets

  while (nP != NULL)
  {
    totalSize += renderSize2(nP, isArray);
    nP = nP->next;
  }

  return totalSize;
}



// -----------------------------------------------------------------------------
//
// arrayLen -
//
static int arrayLen(KjNode* arrayP)
{
  return renderSize(arrayP, KTRUE);
}



// -----------------------------------------------------------------------------
//
// objectLen -
//
static int objectLen(KjNode* arrayP)
{
  return renderSize(arrayP, KFALSE);
}



// -----------------------------------------------------------------------------
//
// kjRender2 -
//
static void kjRender2(Kjson* kjP, RenderBuffer* rBufP, KjNode* nodeP, int indentLevel, KBool inVec)
{
  char* indent = indentGet(kjP, indentLevel);

  if (nodeP->type == KjObject)
  {
    pushString(rBufP, indent);

    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');

      if (kjP->objectStartBracketOnNewLine == KFALSE)
        pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

    if (kjP->objectStartBracketOnNewLine == KTRUE)
    {
      pushString(rBufP, "\n");
      pushString(rBufP, indent);
    }

    KBool objectOnOneLine = KFALSE;

    if (kjP->shortObjectsOnOneLine)
    {
      int objectLength = objectLen(nodeP);

      if (objectLength <= kjP->shortObjectMaxLen)
        objectOnOneLine = KTRUE;
    }
    else if (kjP->objectsOnOneLine == KTRUE)
      objectOnOneLine = KTRUE;

    if (objectOnOneLine == KTRUE)
    {
      char*  savedNlString                    = kjP->nlString;
      KBool  savedObjectStartBracketOnNewLine = kjP->objectStartBracketOnNewLine;
      KBool  savedArrayStartBracketOnNewLine  = kjP->arrayStartBracketOnNewLine;

      kjP->nlString                    = "";
      kjP->objectStartBracketOnNewLine = KFALSE;
      kjP->arrayStartBracketOnNewLine  = KFALSE;
      kjP->spaceAfterComma             = KTRUE;

      pushString(rBufP, "{ ");

      KjNode* nP = nodeP->value.firstChildP;
      int     childNo = 0;  // childNo is for debugging only

      while (nP != NULL)
      {
        kjRender2(kjP, rBufP, nP, 0, KFALSE);  // Note that indent-level is 0
        nP = nP->next;
        ++childNo;
      }

      pushString(rBufP, " }");

      kjP->nlString                    = savedNlString;
      kjP->objectStartBracketOnNewLine = savedObjectStartBracketOnNewLine;
      kjP->arrayStartBracketOnNewLine  = savedArrayStartBracketOnNewLine;
      kjP->spaceAfterComma             = KFALSE;
    }
    else
    {
      pushChar(rBufP, '{');

      if (nodeP->value.firstChildP != NULL)
      {
        pushString(rBufP, kjP->nlString);

        KjNode* nP = nodeP->value.firstChildP;
        int     childNo = 0;  // childNo is for debugging only

        while (nP != NULL)
        {
          kjRender2(kjP, rBufP, nP, indentLevel + 1, KFALSE);
          nP = nP->next;
          ++childNo;
        }
        pushString(rBufP, kjP->nlString);
        pushString(rBufP, indent);
      }

      pushChar(rBufP, '}');
    }
  }
  else if (nodeP->type == KjArray)
  {
    pushString(rBufP, indent);

    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');
      if (kjP->arrayStartBracketOnNewLine == KFALSE)
        pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

    if (kjP->arrayStartBracketOnNewLine == KTRUE)
    {
      pushString(rBufP, "\n");
      pushString(rBufP, indent);
    }

    KBool arrayOnOneLine = KFALSE;

    if (kjP->shortArraysOnOneLine == KTRUE)
    {
      int arrayLength = arrayLen(nodeP);

      if (arrayLength <= kjP->shortArrayMaxLen)
        arrayOnOneLine = KTRUE;
    }
    else if (kjP->arraysOnOneLine == KTRUE)
      arrayOnOneLine = KTRUE;

    if (arrayOnOneLine == KTRUE)
    {
      char*  savedNlString                    = kjP->nlString;
      KBool  savedObjectStartBracketOnNewLine = kjP->objectStartBracketOnNewLine;
      KBool  savedArrayStartBracketOnNewLine  = kjP->arrayStartBracketOnNewLine;

      kjP->nlString                    = "";
      kjP->objectStartBracketOnNewLine = KFALSE;
      kjP->arrayStartBracketOnNewLine  = KFALSE;
      kjP->spaceAfterComma             = KTRUE;

      pushString(rBufP, "[ ");

      KjNode* nP = nodeP->value.firstChildP;
      int     childNo = 0;  // childNo is for debugging only

      while (nP != NULL)
      {
        kjRender2(kjP, rBufP, nP, 0, KTRUE);  // Note that indent-level is 0
        nP = nP->next;
        ++childNo;
      }

      pushString(rBufP, " ]");

      kjP->nlString                    = savedNlString;
      kjP->objectStartBracketOnNewLine = savedObjectStartBracketOnNewLine;
      kjP->arrayStartBracketOnNewLine  = savedArrayStartBracketOnNewLine;
      kjP->spaceAfterComma             = KFALSE;
    }
    else
    {
      pushChar(rBufP, '[');

      if (nodeP->value.firstChildP != NULL)
      {
        pushString(rBufP, kjP->nlString);

        KjNode* nP      = nodeP->value.firstChildP;
        int     childNo = 0;  // childNo is for debugging only

        while (nP != NULL)
        {
          kjRender2(kjP, rBufP, nP, indentLevel + 1, KTRUE);
          nP = nP->next;
          ++childNo;
        }

        pushString(rBufP, kjP->nlString);
        pushString(rBufP, indent);
      }
      pushChar(rBufP, ']');
    }
  }
  else if (nodeP->type == KjString)
  {
    pushString(rBufP, indent);
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');
      pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

    pushChar(rBufP, '"');
    pushString(rBufP, nodeP->value.s);
    pushChar(rBufP, '"');
  }
  else if (nodeP->type == KjInt)
  {
    pushString(rBufP, indent);
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');
      pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

#ifdef USE_VALUE_STRING
    if ((kjP->numbersAsStrings == KTRUE) && (nodeP->valueString != NULL))
      pushString(rBufP, nodeP->valueString);
    else
#endif
      pushInt(rBufP, nodeP->value.i);
  }
  else if (nodeP->type == KjFloat)
  {
    pushString(rBufP, indent);

    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');
      pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

#ifdef USE_VALUE_STRING
    if ((kjP->numbersAsStrings == KTRUE) && (nodeP->valueString != NULL))
      pushString(rBufP, nodeP->valueString);
    else
#endif
      pushFloat(rBufP, nodeP->value.f);
  }
  else if (nodeP->type == KjBoolean)
  {
    pushString(rBufP, indent);
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');
      pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

    pushBool(rBufP, nodeP->value.b);
  }
  else if (nodeP->type == KjNull)
  {
    pushString(rBufP, indent);
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushChar(rBufP, '"');
      pushString(rBufP, kjP->stringBeforeColon);
      pushChar(rBufP, ':');
      pushString(rBufP, kjP->stringAfterColon);  // Typical values: '', ' ', '\n'
    }

    pushString(rBufP, "null");
  }

  if (nodeP->next != NULL)
  {
    pushChar(rBufP, ',');
    if (kjP->spaceAfterComma == KTRUE)
      pushChar(rBufP,  ' ');
    pushString(rBufP, kjP->nlString);
  }
}



// -----------------------------------------------------------------------------
//
// kjRender - render JSON tree to string buffer
//
// FIXME: make kjRender allocate new buffer if 'buf[bufLen]' is not big enough
//        "char* kjRender" - returning the buffer (or 'buf[bufLen]') and the caller
//        must take care of freeing the allocated memory after use.
//
void kjRender(Kjson* kjP, KjNode* nodeP, char* buf, int bufLen)
{
  RenderBuffer  rBuf  = { buf, 0, bufLen };
  RenderBuffer* rBufP = &rBuf;

  if (kjP->spacesPerIndent != 0)
    indentLevelInit(kjP);

  memset(buf, 0, bufLen);  // FIXME: Not necessary if the buffer is correctly zero-terminated after finishing

  if ((nodeP->type != KjObject) && (nodeP->type != KjArray))
  {
    kjRender2(kjP, &rBuf, nodeP, 0, KTRUE);
    return;
  }

  KjNode* childP = nodeP->value.firstChildP;
  KBool   isVec  = (nodeP->type == KjArray)? KTRUE : KFALSE;

  if (isVec == KTRUE)
    pushString(rBufP, "[");
  else
    pushString(rBufP, "{");

  // If children, a newline is appended after the initial '[' or '{'
  if (nodeP->value.firstChildP != NULL)
    pushString(rBufP, kjP->nlString);

  while (childP != NULL)
  {
    kjRender2(kjP, &rBuf, childP, 1, isVec);
    childP = childP->next;
  }

  // If children, a newline is appended before the last ']' or '}'
  if (nodeP->value.firstChildP != NULL)
    pushString(rBufP, kjP->nlString);

  if (isVec == KFALSE)
    pushString(rBufP, "}");
  else
    pushString(rBufP, "]");

  // A last newline is appended at the end
  pushString(rBufP, kjP->nlString);

  // Make sure to NULL terminate string
  rBufP->buf[rBufP->bytesUsed] = 0;
}



// -----------------------------------------------------------------------------
//
// kjRender3 -
//
static void kjRender3(Kjson* kjP, RenderBuffer* rBufP, KjNode* nodeP, KBool inVec)
{
  char* indent = (char*) "";

  if (nodeP->type == KjObject)
  {
    pushString(rBufP, indent);

    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushChar(rBufP, '{');

    KjNode* nP = nodeP->value.firstChildP;

    while (nP != NULL)
    {
      kjRender3(kjP, rBufP, nP, KFALSE);
      nP = nP->next;
    }

    pushChar(rBufP, '}');
  }
  else if (nodeP->type == KjArray)
  {
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushChar(rBufP, '[');

    KjNode* nP = nodeP->value.firstChildP;

    while (nP != NULL)
    {
      kjRender3(kjP, rBufP, nP, KTRUE);
      nP = nP->next;
    }

    pushChar(rBufP, ']');
  }
  else if (nodeP->type == KjString)
  {
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushChar(rBufP, '"');
    pushString(rBufP, nodeP->value.s);
    pushChar(rBufP, '"');
  }
  else if (nodeP->type == KjInt)
  {
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushInt(rBufP, nodeP->value.i);
  }
  else if (nodeP->type == KjFloat)
  {
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushFloat(rBufP, nodeP->value.f);
  }
  else if (nodeP->type == KjBoolean)
  {
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushBool(rBufP, nodeP->value.b);
  }
  else if (nodeP->type == KjNull)
  {
    if (inVec == KFALSE)
    {
      pushChar(rBufP, '"');
      pushString(rBufP, nodeP->name);
      pushString(rBufP, "\":");
    }

    pushString(rBufP, "null");
  }

  if (nodeP->next != NULL)
    pushChar(rBufP, ',');
}



void kjFastRender(Kjson* kjP, KjNode* nodeP, char* buf, int bufLen)
{
  RenderBuffer  rBuf  = { buf, 0, bufLen };
  RenderBuffer* rBufP = &rBuf;

  if ((nodeP->type != KjObject) && (nodeP->type != KjArray))
  {
    kjRender3(kjP, &rBuf, nodeP, KTRUE);
    return;
  }

  KjNode* childP = nodeP->value.firstChildP;
  KBool   isVec  = (nodeP->type == KjArray)? KTRUE : KFALSE;

  if (isVec == KTRUE)
    pushChar(rBufP, '[');
  else
    pushChar(rBufP, '{');

  while (childP != NULL)
  {
    kjRender3(kjP, &rBuf, childP, isVec);
    childP = childP->next;
  }

  if (isVec == KFALSE)
    pushChar(rBufP, '}');
  else
    pushChar(rBufP, ']');

  // NULL terminate string
  rBufP->buf[rBufP->bytesUsed] = 0;
}
