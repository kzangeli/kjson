// 
// FILE            kjReset.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJRESET_H_
#define KJSON_KJRESET_H_

#include "kjson/kjson.h"                // main header file of the library
#include "kjson/KjStatus.h"             // KjStatus



// -----------------------------------------------------------------------------
//
// kjReset - reset the KJson buffer, to get ready for a new parse
//
extern KjStatus kjReset(Kjson* kjP);

#endif  // KJSON_KJRESET_H_
