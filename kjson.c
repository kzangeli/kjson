// 
// FILE            kjson.c - json tool for beautifying/checking json
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <fcntl.h>                           // open, O_RDWR
#include <unistd.h>                          // read, write, close
#include <stdio.h>                           // printf
#include <string.h>                          // strcmp
#include <sys/stat.h>                        // stat
#include <errno.h>                           // errno
#include <sys/select.h>                      // select
#include <stdlib.h>                          // exit

#include "kbase/kMacros.h"                   // K_FT, et al
#include "kbase/kTypes.h"                    // KBool, KFALSE, KTRUE

#include "klog/klInit.h"                     // klInit, ...
#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klComponentRegister.h"        // klComponentRegister
#include "klog/klConfig.h"                   // klConfig
#include "klog/klHooks.h"                    // Log Hooks
#include "klog/klTraceLevelSet.h"            // klTraceLevelSet
#include "klog/klTraceLevelGet.h"            // klTraceLevelGet
#include "klog/klComponentLookup.h"          // klComponentLookup
#include "klog/kLog.h"                       // K Log Macros

#include "kalloc/kaBufferInit.h"             // kaBufferInit
#include "kalloc/kaBufferReset.h"            // kaBufferReset

#include "kjson/KjTraceLevels.h"             // kjTraceLevelInfo
#include "kjson/kjson.h"                     // kjson library
#include "kjson/kjRender.h"                  // kjRender
#include "kjson/kjConfig.h"                  // kjConfig
#include "kjson/kjParse.h"                   // kjParse
#include "kjson/kjSax.h"                     // kjSaxEventName
#include "kjson/kjBufferCreate.h"            // kjBufferCreate



// -----------------------------------------------------------------------------
//
// ExitCodes - 
//
enum ExitCodes
{
  KjxUsage                  = 1,
  KjxStdinSelectError       = 2,
  KjxStdinReadError         = 3,
  KjxNoJsonBuffer           = 4,
  KjxInvalidOption          = 5,
  KjxCantOpenOutputFile     = 6,
  KjxCantWriteToOutputFile  = 7,
};



// -----------------------------------------------------------------------------
//
// Trace Levels for the test program (kjson lib uses its own trace levels)
//
typedef enum TestTraceLevel
{
  TtlUnitTest,
  TtlFuncTest
} TestTraceLevel;



// -----------------------------------------------------------------------------
//
// traceLevelInfo
//
static KlComponentTraceLevelInfo traceLevelInfo[] =
{
  { TtlUnitTest,    "Unit Test"        },
  { TtlFuncTest,    "Func Test"        }
};



// -----------------------------------------------------------------------------
//
// progName -
//
char* progName = "kjson";



// -----------------------------------------------------------------------------
//
// Command line arguments
//
char*   indentStep                = NULL;
KBool   onelineOutput             = KFALSE;
KBool   spaceBeforeColon          = KFALSE;
KBool   noSpaceAfterColon         = KFALSE;
KBool   objectStartOnNewLine      = KFALSE;
KBool   arrayStartOnNewLine       = KFALSE;
KBool   arraysOnOneLine           = KFALSE;
KBool   shortArraysOnOneLine      = KFALSE;
KBool   objectsOnOneLine          = KFALSE;
KBool   shortObjectsOnOneLine     = KFALSE;
KBool   minimized                 = KFALSE;
KBool   toStderr                  = KFALSE;
char*   outFile                   = NULL;
char*   jsonFile                  = NULL;
KBool   sortAlphabetically        = KFALSE;
KBool   sortAlphabeticallyReverse = KFALSE;
KBool   verbose                   = KFALSE;
KBool   parseNumbers              = KFALSE;
char*   shortArrayMaxLen          = NULL;
char*   shortObjectMaxLen         = NULL;
KBool   saxTest                   = KFALSE;
KBool   kjVerbose                 = KFALSE;
KBool   ktest                     = KFALSE;



// -----------------------------------------------------------------------------
//
// input buffer size definitions
//
#ifndef KJSON_RENDER_MAX_MB
#define KJSON_RENDER_MAX_MB     64
#endif
#define BUF_SIZE   (KJSON_RENDER_MAX_MB * 1024 * 1024)



// -----------------------------------------------------------------------------
//
// jsonBuf - the buffer that stores the input JSON
//
char jsonBuf[BUF_SIZE];



// -----------------------------------------------------------------------------
//
// testLog - 
//
KlComponent* testLog = NULL;



// -----------------------------------------------------------------------------
//
// usage - 
//
static void usage(void)
{
  char* empty = strdup(progName);
  char* eP    = empty;
  
  while (*eP)
  {
    *eP = ' ';
    ++eP;
  }

  printf("%s [-u (show this text)]\n"
         "%s [-t (trace levels for kjson utility)]\n"
         "%s [-T (global trace levels)]\n"
         "%s [-v (verbose mode for kjson utility)]\n"
         "%s [--klVerbose (verbose mode for klog library)]\n"
         "%s [--kjVerbose (verbose mode for kjson library)]\n"
         "%s [-V (global verbose mode)]\n"
         "%s [--fixme (turn on FIXME log)]\n"
         "%s [-i (spaces per indent)]\n"
         "%s [-ol (one-line output)]\n"
         "%s [-sbc (space before colon)]\n"
         "%s [-nsac (no space after colon)]\n"
         "%s [-onl (object start bracket on new line)]\n"
         "%s [-anl (array start bracket on new line)]\n"
         "%s [-aol (arrays on one line)\n"
         "%s [-saol (short arrays (80 rendered chars) on one line)\n"
         "%s [-saml <len> (change the length for an array to be considered short - 80 is default)]\n"
         "%s [-ool (objects on one line)\n"
         "%s [-sool (short objects (80 rendered chars) on one line)\n"
         "%s [-soml <len> (change the length for an object to be considered short - 80 is default)]\n"
         "%s [-min (minimized - no whitespace at all)]\n"
         "%s [-pn (do parse numbers)]\n"
         "%s [-sr (output to stderr)]\n"
         "%s [-o <output file>]\n"
         "%s [-sort (sort object members alphabetically)]\n"
         "%s [-rsort (sort object members in reverse alphabetic order)]\n"
         "%s [-saxTest (print a log line for each SAX event)]\n"
         "%s <json-file>\n",
         progName,
         empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty,
         empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty, empty);

  exit(KjxUsage);
}



// -----------------------------------------------------------------------------
//
// parseArgs -
//
void parseArgs(int argC, char* argV[])
{
  int ix = 1;

  KL_V(testLog, ("In parseArgs"));
  while (ix < argC)
  {
    KL_V(testLog, ("arg %d: %s", ix, argV[ix]));
    if (strcmp(argV[ix], "-u") == 0)
    {
      usage();
      exit(KjxUsage);
    }
    else if (strcmp(argV[ix], "--ktest") == 0)
      ktest = KTRUE;
    else if (strcmp(argV[ix], "--kjVerbose") == 0)
    {
      kjVerbose = KTRUE;
    }
    else if (strcmp(argV[ix], "-T") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing trace level string after -T\n", progName);
        usage();
        exit(KjxInvalidOption);
      }
      
      klTraceLevelSet(NULL, argV[ix]);
    }
    else if (strcmp(argV[ix], "-t") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing trace level string after -t\n", progName);
        usage();
        exit(KjxInvalidOption);
      }
      
      klTraceLevelSet(testLog, argV[ix]);
    }
    else if (strcmp(argV[ix], "-V") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing trace level string after -V\n", progName);
        usage();
        exit(KjxInvalidOption);
      }

      // FIXME: Implement klVerboseSet(NULL, "kjson,ktree,...");
      // klVerboseSet(NULL, argV[ix]);
    }
    else if (strcmp(argV[ix], "-v") == 0)
    {
      KL_V(testLog, ("Got -v"));
      klConfig(testLog, KlcVerbose, (void*) KTRUE);
      KL_V(testLog, ("Can you see me?"));
    }
    else if (strcmp(argV[ix], "-min") == 0)
      minimized = KTRUE;
    else if (strcmp(argV[ix], "-i") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing integer-value after -i\n", progName);
        usage();
        exit(KjxInvalidOption);
      }

      indentStep = argV[ix];
    }
    else if (strcmp(argV[ix], "-ol") == 0)
      onelineOutput = KTRUE;
    else if (strcmp(argV[ix], "-sbc") == 0)
      spaceBeforeColon = KTRUE;
    else if (strcmp(argV[ix], "-nsac") == 0)
      noSpaceAfterColon = KTRUE;
    else if (strcmp(argV[ix], "-onl") == 0)
      objectStartOnNewLine = KTRUE;
    else if (strcmp(argV[ix], "-anl") == 0)
      arrayStartOnNewLine = KTRUE;
    else if (strcmp(argV[ix], "-aol") == 0)
      arraysOnOneLine = KTRUE;
    else if (strcmp(argV[ix], "-saol") == 0)
      shortArraysOnOneLine = KTRUE;
    else if (strcmp(argV[ix], "-saml") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing integer-value after -saml\n", progName);
        usage();
        exit(KjxInvalidOption);
      }
      
      shortArrayMaxLen = argV[ix];
      KL_V(testLog, ("shortArrayMaxLen == '%s'", shortArrayMaxLen));
    }
    else if (strcmp(argV[ix], "-soml") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing integer-value after -soml\n", progName);
        usage();
        exit(KjxInvalidOption);
      }
      
      shortObjectMaxLen = argV[ix];
    }
    else if (strcmp(argV[ix], "-ool") == 0)
      objectsOnOneLine = KTRUE;
    else if (strcmp(argV[ix], "-sool") == 0)
      shortObjectsOnOneLine = KTRUE;
    else if (strcmp(argV[ix], "-pn") == 0)
      parseNumbers = KTRUE;
    else if (strcmp(argV[ix], "-sort") == 0)
      sortAlphabetically = KTRUE;
    else if (strcmp(argV[ix], "-rsort") == 0)
      sortAlphabeticallyReverse = KTRUE;
    else if (strcmp(argV[ix], "-sr") == 0)
      toStderr = KTRUE;
    else if (strcmp(argV[ix], "-o") == 0)
    {
      ++ix;
      if (ix >= argC)
      {
        fprintf(stderr, "%s: missing file-name after -o\n", progName);
        usage();
        exit(KjxInvalidOption);
      }
      
      outFile = argV[ix];
    }
    else if (strcmp(argV[ix], "-saxTest") == 0)
    {
      saxTest = KTRUE;
    }
    else
    {
      // Not a recognized option - must be the json-file
      if (jsonFile != NULL)
      {
        fprintf(stderr, "%s: invalid option (or second JSON file): '%s'\n", progName, argV[ix]);
        usage();
        exit(KjxInvalidOption);
      }

      jsonFile = argV[ix];
    }

    ++ix;
  }

  //
  // Check CLI options
  //
  if ((sortAlphabetically == KTRUE) && (sortAlphabeticallyReverse == KTRUE))
  {
    fprintf(stderr, "%s: -sort and -rsort can't both be set\n", progName);
    exit(KjxInvalidOption);
  }
}



// -----------------------------------------------------------------------------
//
// outputToFile -
//
static void outputToFile(char* outFile, char* rBuf)
{
  int fd = open(outFile, O_RDWR | O_TRUNC | O_CREAT, 0644);

  if (fd == -1)
  {
    fprintf(stderr, "%s: can't open '%s': %s\n", progName, outFile, strerror(errno));
    exit(KjxCantOpenOutputFile);
  }

  int bytes  = strlen(rBuf);
  int nb     = 0;
  int sz;

  while (nb < bytes)
  {
    sz = write(fd, &rBuf[nb], bytes - nb);
    if (sz == -1)
    {
      fprintf(stderr, "%s: can't write to '%s': %s\n", progName, outFile, strerror(errno));
      exit(4);
    }

    nb += sz;
  }

  close(fd);
  chmod(outFile, 0644);
}



// -----------------------------------------------------------------------------
//
// outputBuffer -
//
static char outputBuffer[BUF_SIZE];



// -----------------------------------------------------------------------------
//
// bufPopulateFromFile -
//
int bufPopulateFromFile(char* jsonFile, char* buf, int bufSize)
{
  struct stat s;
  if (stat(jsonFile, &s) != 0)
  {
    fprintf(stderr, "%s: stat(%s): %s\n", progName, jsonFile, strerror(errno));
    exit(51);
  }
  
  if (s.st_size > bufSize)
  {
    fprintf(stderr, "%s: json file '%s' too big (max is %dMb)\n", progName, jsonFile, KJSON_RENDER_MAX_MB);
    exit(52);
  }

  int   nb;
  int   sz = s.st_size;
  int   fd;
  
  if ((fd = open(jsonFile, O_RDONLY)) == -1)
  {
    fprintf(stderr, "%s: open '%s': %s\n", progName, jsonFile, strerror(errno));
    exit(4);
  }

  if ((nb = read(fd, jsonBuf, sz)) != sz)
  {
    fprintf(stderr, "%s: reading from '%s': %s\n", progName, jsonFile, strerror(errno));
    exit(5);
  }

  return nb;
}



// -----------------------------------------------------------------------------
//
// pipeRead - 
//
int pipeRead(char* buf, int bufLen)
{
  int    fds;
  fd_set rFds;

  KL_V(testLog, ("Anything to read on stdin?"));

  while (1)
  {
    struct timeval tv = { 0, 10000 };  // poll

    FD_ZERO(&rFds);
    FD_SET(0, &rFds);

    fds = select(1, &rFds, NULL, NULL, &tv);
    if ((fds == -1) && (errno != EINTR))
    {
      fprintf(stderr, "%s: select on stdin: %s\n", progName, strerror(errno));
      exit(KjxStdinSelectError);
    }
    else if (fds == 0)  // Nothing to read on ANY fd (only stdin in fd-set ... :-)) - return 0 (bytes)
    {
      return 0;
    }
    else if (FD_ISSET(0, &rFds))  // Something to read on fd 0
      break;

    // else, errno == EINTR => try again
  }

  // Read from stdin
  int nb;
  nb = read(0, buf, bufLen);
  if (nb == -1)
  {
    fprintf(stderr, "%s: read on stdin: %s\n", progName, strerror(errno));
    exit(KjxStdinReadError);
  }

  return nb;
}


static int indentLevel = 0;
// -----------------------------------------------------------------------------
//
// saxTestFunction - 
//
static int saxTestFunction(Kjson* kjP, KjSaxEvent event, char* name, KjValue* valueP, KBool inArray)
{
  char indent[256];
  int  indents = 0;

  static int  calls  = 0;
  ++calls;

  if (event == KjSaxError)
  {
#ifdef KJ_LINE_NUMBERS
    printf("Error detected during parse (call %d, line %d, pos %d): %s\n", calls, kjP->lineNo, kjP->errorPos, kjP->errorString);
#else
    printf("Error detected during parse (call %d, pos %d): %s\n", calls, kjP->errorPos, kjP->errorString);
#endif
    return 1;
  }

  if ((event == KjObjectEnd) || (event == KjArrayEnd))
    indentLevel -= 2;

  while ((indents < indentLevel) && (indents < sizeof(indent) - 1))
  {
    indent[indents] = ' ';
    ++indents;
  }
  indent[indents] = 0;
  
  if (event == KjSaxStart)
  {
    printf("----- SAX Start -----\n");
    return 0;
  }

  if (event == KjSaxEnd)
  {
    printf("----- SAX End -----\n");
    return 0;
  }
  
  printf("%s%s", indent, kjSaxEventName(event));

  if (inArray == KFALSE)
    printf(" (%s)", name);

  switch (event)
  {
  case KjStringValue:
    printf(": '%s'", valueP->s);
    break;

  case KjFloatValue:
    if (kjP->numbersAsStrings == KTRUE)
      printf(": %s", valueP->s);
    else
      printf(": %f", valueP->f);
    break;
    
  case KjIntegerValue:
    if (kjP->numbersAsStrings == KTRUE)
      printf(": %s", valueP->s);
    else
      printf(": %lld", valueP->i);
    break;
    
  case KjBoolValue:
    printf(": %s", K_FT(valueP->b));
    break;

  case KjNullValue:
    printf(": null");
    break;

  default:
    break;
  }

  printf("\n");

  if ((event == KjObjectStart) || (event == KjArrayStart))
    indentLevel += 2;

  return 0;
}



// -----------------------------------------------------------------------------
//
// logErrorHookStatus - status of last error reported to error hook function
//
KlStatus  logErrorHookStatus = KlsOk;
char*     logErrorHookText   = NULL;



// -----------------------------------------------------------------------------
//
// logErrorHook -
//
static void logErrorHook(void* vP, KlStatus ks, char* errorDescription, const char* file, int lineNo, const char* fName)
{
  // KBL_M(("%s[%d]/logErrorHook: status %s (%d). error text: %s", file, lineNo, klStatus(ks), ks, errorDescription));
  logErrorHookStatus  = ks;
  logErrorHookText    = errorDescription;
}



char kallocBuffer[128 * 1024];
// -----------------------------------------------------------------------------
//
// main - 
//
int main(int argC, char* argV[])
{
  KlStatus      kls;
  char*         progName = "kjson";

  //
  // Initialize logging library
  //
  if ((kls = klInit(progName, NULL, KTRUE, NULL)) != KlsOk)
    KBL_RE(1, ("klInit: %s", klStatus(kls)));
  if ((kls = klConfig(NULL, KlcErrorHook, logErrorHook)) != KlsOk)
    KBL_RE(2, ("klConfig(KlcErrorHook): %s", klStatus(kls)));

  Kjson  kjson;
  KAlloc kalloc;
  Kjson* kjP;
  
  kaBufferInit(&kalloc, kallocBuffer, sizeof(kallocBuffer), 16 * 1024, NULL, (char*) "KJSON Alloc Buffer");
  
  kjP = kjBufferCreate(&kjson, &kalloc);
  
    
  //
  // After all lib-init, we register the klog component. Nicer like that ...
  //
  if ((testLog = klComponentRegister("test", K_VEC_SIZE(traceLevelInfo), traceLevelInfo)) == NULL)
    KBL_RE(4, ("klComponentRegister: %s", logErrorHookText));
  
  parseArgs(argC, argV);

  //
  // Line numbers in log file often change and make the diff more complicated.
  // So, for 'ktest' tests, line numbers are switched off - always shown as ZERO.
  //
  if (ktest == KTRUE)
  {
    kbNoLineNumbers = KTRUE;
    kbVerbose       = KFALSE;
  }
  else
  {
    if ((kls = klConfig(testLog, KlcLogToScreen, NULL)) != KlsOk)
      KBL_RE(5, ("klConfig(LogToScreen): %s", klStatus(kls)));
    if ((kls = klConfig(testLog, KlcErrorsToStderr, NULL)) != KlsOk)
      KBL_RE(6, ("klConfig(ErrorsToStderr): %s", klStatus(kls)));
  }
  
#if 0  
  // <TMP>
  char buf[256];

  klTraceLevelGet(testLog, buf, sizeof(buf));
  printf("tracelevels for 'test': %s\n", buf);

  KlComponent* kjsonLog = klComponentLookup("kjson");
  klTraceLevelGet(kjsonLog, buf, sizeof(buf));
  printf("tracelevels for 'kjson': %s\n", buf);
  exit(1);
  // </TMP>
#endif
  

  //
  // json input can come either via stdin (pipe) or via a file (as CLI parameter)
  // As we don't want and CLI option for usage via pipe, we must always check data
  // on stdin.
  // Later on, if data comes in both via stdin and a file as CLI parameter,
  // we flag an error and die.
  //
  int bufLen = pipeRead(jsonBuf, sizeof(jsonBuf));


  //
  // input buffer both from stdin and file?
  //
  if ((bufLen != 0) && (jsonFile != NULL))
    KL_X(testLog, 7, ("JSON contents via pipe AND via file as argument (%s) is not allowed", jsonFile));

  //
  // Input buffer from file?
  //
  if (jsonFile != NULL)
  {
    // Read contents of the file 'jsonFile' and dump into the buffer 'jsonBuf'
    if (jsonFile == NULL)
    {
      KL_E(testLog, ("no JSON data given (use either CLI parameter or pipe the data via stdin"));
      usage();
      exit(8);
    }

    bufLen = bufPopulateFromFile(jsonFile, jsonBuf, sizeof(jsonBuf));
  }
  else
    jsonFile = "pipe";

  //
  // Now, do we have any data?
  //
  if (bufLen <= 0)
    KL_X(testLog, 9, ("no JSON buffer to examine"));

  
  //
  // Configure kjson library
  //
  if (minimized == KTRUE)
    kjConfig(kjP, KjcMinimized, NULL);

  if (indentStep != NULL)
    kjConfig(kjP, KjcIndentStep, indentStep);

  if (onelineOutput == KTRUE)
  {
    kjConfig(kjP, KjcNewlineString, "");
    kjConfig(kjP, KjcIndentStep, "0");
  }
  
  if (spaceBeforeColon == KTRUE)
    kjConfig(kjP, KjcSpaceBeforeColon, NULL);

  if (noSpaceAfterColon == KTRUE)
    kjConfig(kjP, KjcNoSpaceAfterColon, NULL);

  if (objectStartOnNewLine == KTRUE)
    kjConfig(kjP, KjcObjectStartOnNewLine, NULL);

  if (arrayStartOnNewLine == KTRUE)
    kjConfig(kjP, KjcArrayStartOnNewLine, NULL);

  if (arraysOnOneLine == KTRUE)
    kjConfig(kjP, KjcArraysOnOneLine, NULL);

  if (shortArraysOnOneLine == KTRUE)
    kjConfig(kjP, KjcShortArraysOnOneLine, NULL);

  if (objectsOnOneLine == KTRUE)
    kjConfig(kjP, KjcObjectsOnOneLine, NULL);

  if (shortObjectsOnOneLine == KTRUE)
    kjConfig(kjP, KjcShortObjectsOnOneLine, NULL);

  if (shortArrayMaxLen != NULL)
    kjConfig(kjP, KjcShortArrayMaxLen, shortArrayMaxLen);

  if (shortObjectMaxLen != NULL)
    kjConfig(kjP, KjcShortObjectMaxLen, shortObjectMaxLen);

  //
  // Special treatment for numbers when beautifying
  // Numbers aren't parsed, calculated and stored as an int/float.
  // Instead, the chars representing the numbers are pointer to at a string
  // and when rendering the 'Number', its string representation is rendered instead.
  // This way we have 0 problems with rounding errors and the parsing stage is faster too.
  // Only down-side is that overflows will not be detected.
  // Perhaps this not a bad thing. Not the beautifiers job to check for overflow ...
  //
  // Now, to tell kjson to NOT parse numbers, kjConfig is called with
  // 'KjcNumbersAsStrings' set to 'Yes'.
  //
  if (parseNumbers == KFALSE)
    kjConfig(kjP, KjcNumbersAsStrings, "Yes");
  else
    kjConfig(kjP, KjcNumbersAsStrings, "No");

  if (sortAlphabetically == KTRUE)
    kjConfig(kjP, KjcSorted, NULL);

  if (sortAlphabeticallyReverse == KTRUE)
    kjConfig(kjP, KjcSortedReverse, NULL);

  if (saxTest == KTRUE)
    kjConfig(kjP, KjcSaxFunction, (char*) saxTestFunction);

  //
  // Parse JSON buffer
  //
  KL_V(testLog, ("Calling kjParse"));
  KjNode* top = kjParse(kjP, jsonBuf);
  
  if (top == NULL)
  {
#ifdef KJ_LINE_NUMBERS
    fprintf(stderr, "%s: %s[%d]: %s (offending buffer position: %d)\n", progName, jsonFile, kjP->lineNo, kjP->errorString, kjP->errorPos);
#else
    fprintf(stderr, "%s: %s: %s (offending buffer position: %d)\n", progName, jsonFile, kjP->errorString, kjP->errorPos);
#endif
    exit(10);
  }

  KL_V(testLog, ("Back from kjParse"));

  //
  // Render output to file/stdout/stderr
  //
  if (saxTest == KTRUE)  // No render if SAX test - output already out there
    return 0;

  KL_V(testLog, ("Calling kjRender"));
  kjRender(kjP, top, outputBuffer, sizeof(outputBuffer));
  KL_V(testLog, ("Back from kjRender"));

  if (outFile != NULL)
    outputToFile(outFile, outputBuffer);
  else if (toStderr == KTRUE)
    fprintf(stderr, "%s\n", outputBuffer);
  else
    printf("%s\n", outputBuffer);

  kaBufferReset(&kalloc, KFALSE);
  return 0;
}
