// 
// FILE            kjRender.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJ_RENDER_H_
#define KJSON_KJ_RENDER_H_

#include "kjson/kjson.h"       // Kjson struct



// -----------------------------------------------------------------------------
//
// kjRender - render JSON tree to string buffer
//
extern void kjRender(Kjson* kjP, KjNode* nodeP, char* buf, int bufLen);
extern void kjFastRender(Kjson* kjP, KjNode* nodeP, char* buf, int bufLen);

#endif  // KJSON_KJ_RENDER_H_
