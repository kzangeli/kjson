// 
// FILE            kjClone.c - clone a KjNode
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // NULL
#include <string.h>                     // strcmp

#include "kbase/kLibLog.h"              // K Log macros
#include "kbase/kBasicLog.h"            // K BasicLog macros
#include "kjson/kjson.h"                // Kjson
#include "kjson/KjNode.h"               // KjNode
#include "kjson/kjBuilder.h"            // kjString, kjArray, kjFloat, ...
#include "kjson/kjClone.h"              // Own Interface



// -----------------------------------------------------------------------------
//
// kjClone - clone a node
//
KjNode* kjClone(Kjson* kjP, KjNode* nodeP)
{
  KjNode* newNodeP = NULL;

  switch (nodeP->type)
  {
  case KjString:
    newNodeP = kjString(kjP, nodeP->name, nodeP->value.s);
    break;

  case KjInt:
    newNodeP = kjInteger(kjP, nodeP->name, nodeP->value.i);
    break;

  case KjFloat:
    newNodeP = kjFloat(kjP, nodeP->name, nodeP->value.f);
    break;

  case KjBoolean:
    newNodeP = kjBoolean(kjP, nodeP->name, nodeP->value.b);
    break;

  case KjNull:
    newNodeP = kjNull(kjP, nodeP->name);
    break;

  case KjArray:
    newNodeP = kjArray(kjP, nodeP->name);

    if (newNodeP != NULL)
    {
      KjNode* arrItemP;

      for (arrItemP = nodeP->value.firstChildP; arrItemP != NULL; arrItemP = arrItemP->next)
      {
        KjNode* cloneP = kjClone(kjP, arrItemP);

        if (cloneP == NULL)
        {
          KLOG_E("Error cloning array item - out of memory?");
          return NULL;
        }

        kjChildAdd(newNodeP, cloneP);
      }
    }
    break;
  
  case KjObject:
    newNodeP = kjObject(kjP, nodeP->name);

    if (newNodeP != NULL)
    {
      KjNode* itemP;

      for (itemP = nodeP->value.firstChildP; itemP != NULL; itemP = itemP->next)
      {
        KjNode* cloneP = kjClone(kjP, itemP);

        if (cloneP == NULL)
        {
          KLOG_E("Error cloning KjNode '%s' - out of memory?", itemP->name);
          return NULL;
        }

        kjChildAdd(newNodeP, cloneP);
      }
    }
    break;

  case KjNone:
  default:
    KLOG_E("Invalid KJSON Value Type for node '%s'", nodeP->name);
    return NULL;
  }

  if (newNodeP == NULL)
    KLOG_E("Error cloning KjNode '%s' - out of memory?", nodeP->name);

#ifdef USE_CHAR_SUM
  else if (newNodeP->name != NULL)
  {
    char* s;

    // Calculating char-sum over the name
    newNodeP->cSum = 0;
    for (s = newNodeP->name; *s != 0; s++)
      newNodeP->cSum += *s;
  }
#endif

  return newNodeP;
}
