# 
# FILE            makefile
# 
# AUTHOR          Ken Zangelin
# 
# Copyright 2019 Ken Zangelin
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with theLicense.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
LIB_SO        = libkjson.so
LIB           = libkjson.a
CC            = gcc
CCCC          = g++
INCLUDE       = -I.. -I /usr/include/libbson-1.0
DFLAGS        = -DANSI -DUSE_CHAR_SUM=1 # -DKJ_LOG_ON
CFLAGS        = -g -Wall -fPIC -Wno-unused-function -fstack-protector-all $(DFLAGS) $(INCLUDE)
LIB_SOURCES   = kjBuilder.c       \
                kjBufferCreate.c  \
                kjParse.c         \
                kjRender.c        \
                KjStatus.c        \
                KjNode.c          \
                kjFree.c          \
                kjConfig.c        \
                kjLookup.c        \
                kjReset.c         \
                kjSax.c           \
                kjsonVersion.c    \
                kjClone.c
LIB_OBJS      = $(LIB_SOURCES:c=o)

TOOL          = kjson
TOOL_SOURCES  = kjson.c
TOOL_OBJS     = $(TOOL_SOURCES:c=o)

TEST1         = ktime
TEST1_SOURCES = ktime.cpp
TEST1_OBJS    = $(TEST1_SOURCES:cpp=o)

TEST2         = kjBuildTest
TEST2_SOURCES = kBuildTest.c
TEST2_OBJS    = $(TEST2_SOURCES:c=o)

DOC01         = docExample01
DOC01_SOURCES = docExample01.c
DOC01_OBJS    = $(DOC01_SOURCES:c=o)

DOC02         = docExample02
DOC02_SOURCES = docExample02.c
DOC02_OBJS    = $(DOC02_SOURCES:c=o)

LIBS          = ../kalloc/libkalloc.a ../kbase/libkbase.a ../klog/libklog.a -lpthread -lrt

all: $(LIB_SO) $(LIB) $(TOOL) $(DOC01) $(DOC02)

test: $(TEST1) $(TEST2)

clean:
						rm -f *.o
						rm -f *.a
						rm -f *~
						rm -f *.so
						rm -f $(TOOL) $(TEST1) $(TEST2) $(DOC01) $(DOC02)

install:    all
						mkdir -p bin
						cp $(TOOL) bin/

di:         install

ci:         clean install

$(LIB):			$(LIB_OBJS) $(LIB_SOURCES)
						ar r $(LIB) $(LIB_OBJS)
						ranlib $(LIB)

$(LIB_SO):	$(LIB_OBJS) $(LIB_SOURCES)
						$(CC) -shared $(LIB_OBJS) -o $(LIB_SO)

$(TOOL):		$(TOOL_OBJS) $(LIB)
						$(CC) -o $(TOOL) $(TOOL_OBJS) $(LIB) $(LIBS)

$(TEST1):		$(TEST1_OBJS) $(LIB)
						$(CCCC) -o $(TEST1) $(TEST1_OBJS) $(LIB) $(LIBS)

$(TEST2):		$(TEST2_OBJS) $(LIB)
						$(CC) -o $(TEST2) $(TEST2_OBJS) $(LIB) $(LIBS)

$(DOC01):		$(DOC01_OBJS) $(LIB)
						$(CC) -o $(DOC01) $(DOC01_OBJS) $(LIB) $(LIBS)

$(DOC02):		$(DOC02_OBJS) $(LIB)
						$(CC) -o $(DOC02) $(DOC02_OBJS) $(LIB) $(LIBS)

%.o: %.c
						$(CC) $(CFLAGS) -c $^ -o $@

%.o: %.cpp
						$(CCCC) $(CFLAGS) -c $^ -o $@

%.i: %.c
						$(CC) $(CFLAGS) -c $^ -E > $@

%.i: %.cpp
						$(CCCC) $(CFLAGS) -c $^ -E > $@

%.cs: %.c
						$(CC) -Wa,-adhln -g $(CFLAGS)  $^  > $@

