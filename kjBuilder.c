// 
// FILE            kjBuilder.c - build a JSON tree
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                           // NULL
#include <string.h>                          // strcmp
#include <stdlib.h>                          // malloc

#include "kbase/kTypes.h"                    // KBool, KFALSE, KTRUE
#include "kbase/kLibLog.h"                   // K Log macros
#include "kalloc/kaAlloc.h"                  // kaAlloc

#include "kjson/KjTraceLevels.h"             // KJ Log Trace Levels
#include "kjson/kjson.h"                     // Kjson struct
#include "kjson/KjNode.h"                    // KjNode
#include "kjson/kjBuilder.h"                 // Own Interface


// #define USE_CHAR_POINTERS 1



// -----------------------------------------------------------------------------
//
// kjObject - create a kjson object
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the object
//
// RETURN VALUE
//   `kjObject` returns a KjNode of `KjObject` type.
//
KjNode* kjObject(Kjson* kjP, const char* name)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen = (name == NULL)? 0 : strlen(name);
  int     bufLen  = sizeof(KjNode) + nameLen + 1;
#endif
  char*   buf     = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP   = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type              = KjObject;
  nodeP->next              = NULL;
  nodeP->value.firstChildP = NULL;
  nodeP->lastChild         = NULL;

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjArray - create a kjson array
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the array
//
// RETURN VALUE
//   `kjArray` returns a KjNode of `KjArray` type.
//
KjNode* kjArray(Kjson* kjP, const char* name)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen = (name == NULL)? 0 : strlen(name);
  int     bufLen  = sizeof(KjNode) + nameLen + 1;
#endif
  char*   buf     = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP   = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type              = KjArray;
  nodeP->next              = NULL;
  nodeP->value.firstChildP = NULL;
  nodeP->lastChild         = NULL;

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjString - 
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the string field
//   value - the content of the string
//
// RETURN VALUE
//   `kjString` returns a KjNode of `KjString` type with the contents from the third parameter `value`
//
//
// FIXME: Include the 'value' in the malloc of 'buf' as well
//
KjNode* kjString(Kjson* kjP, const char* name, const char* value)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen  = (name  != NULL)?  strlen(name) : 0;
  int     valueLen = (value != NULL)?  strlen(value) : 0;
  int     bufLen   = sizeof(KjNode) + nameLen + 1 + valueLen + 1;
#endif
  char*   buf      = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP    = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type        = KjString;
#ifdef USE_VALUE_STRING
  nodeP->valueString = NULL;
#endif
  nodeP->next        = NULL;

  if (value != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->value.s = (char*) value;
#else
    nodeP->value.s = &buf[sizeof(KjNode) + nameLen + 1];
    strcpy(nodeP->value.s, value);
#endif
  }
  else
  {
    nodeP->value.s = NULL;
  }

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjInteger - 
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the integer field
//   value - the value of the integer field
//
// RETURN VALUE
//   `kjInteger` returns a KjNode of `KjInt` type with the value of the third parameter `value`
//
KjNode* kjInteger(Kjson* kjP, const char* name, long long value)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen = (name == NULL)? 0 : strlen(name);
  int     bufLen  = sizeof(KjNode) + nameLen + 1;
#endif
  char*   buf     = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP   = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type     = KjInt;
  nodeP->value.i  = value;

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjFloat - 
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the field
//   value - the value of the field
//
// RETURN VALUE
//   `kjFloat` returns a KjNode of `KjFloat` type with the value of the third parameter `value`
//
KjNode* kjFloat(Kjson* kjP, const char* name, double value)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen = (name == NULL)? 0 : strlen(name);
  int     bufLen  = sizeof(KjNode) + nameLen + 1;
#endif
  char*   buf     = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP   = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type     = KjFloat;
  nodeP->value.f  = value;

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjNull - 
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the field
//
// RETURN VALUE
//   `kjNull` returns a KjNode of `KjNull` type.
//
KjNode* kjNull(Kjson* kjP, const char* name)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen = (name == NULL)? 0 : strlen(name);
  int     bufLen  = sizeof(KjNode) + nameLen + 1;
#endif
  char*   buf     = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP   = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type = KjNull;

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjBoolean - 
//
// PARAMETERS
//   kjP   - unused (see issue #1)
//   name  - the name of the boolean field
//   value - the value of the boolean field
//
// RETURN VALUE
//   `kjBoolean` returns a KjNode of `KjBoolean` type with the value of the third parameter `value`
//
KjNode* kjBoolean(Kjson* kjP, const char* name, KBool value)
{
#if USE_CHAR_POINTERS
  int     bufLen  = sizeof(KjNode);
#else
  int     nameLen = (name == NULL)? 0 : strlen(name);
  int     bufLen  = sizeof(KjNode) + nameLen + 1;
#endif
  char*   buf     = (kjP != NULL)? kaAlloc(kjP->kallocP, bufLen) : malloc(bufLen);
  KjNode* nodeP   = (KjNode*) buf;

  if (nodeP == NULL)
    KLOG_RE(NULL, "malloc failed to allocate %d bytes", bufLen);

  if (name != NULL)
  {
#if USE_CHAR_POINTERS
    nodeP->name = (char*) name;
#else
    nodeP->name = &buf[sizeof(KjNode)];
    strcpy(nodeP->name, name);  // 'name' fits, as the malloc was done with the strlen of 'name' in mind
#endif
  }
  else
    nodeP->name = NULL;

  nodeP->type     = KjBoolean;
  nodeP->value.b  = value;
  
  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjChildAdd - add a node to a container
//
// PARAMETERS
//   container - pointer to the father (container) of the child to be added
//   child     - pointer to the child to be added
//
// NOTE
//   The child is appended to the list of children.
//   If 'child' is already part of a linked list, it will be removed from that list.
//
void kjChildAdd(KjNode* container, KjNode* child)
{
  // First child?
  if (container->value.firstChildP != NULL)
    container->lastChild->next = child;
  else
    container->value.firstChildP  = child;
  
  container->lastChild = child;

  child->next = NULL;
}



// -----------------------------------------------------------------------------
//
// kjChildRemove - remove a node from a container
//
// PARAMETERS
//   container - pointer to the father (container) of the child to be removed
//   child     - pointer to the child to be removed
//
void kjChildRemove(KjNode* container, KjNode* child)
{
  KjNode* prevP = NULL;
  KjNode* nodeP = container->value.firstChildP;

  while (nodeP != child)
  {
    if (nodeP == NULL)
      return;  // Not found

    prevP = nodeP;
    nodeP = nodeP->next;
  }

  if (nodeP == container->value.firstChildP)  // Hit in beginning of list
  {
    container->value.firstChildP = nodeP->next;

    if (container->lastChild == nodeP)  // Only ONE item in list?
      container->lastChild = NULL;
  }
  else if (nodeP == container->lastChild)  // Hit in end of list
  {
    container->lastChild = prevP;
    prevP->next          = NULL;
  }
  else  // Hit in the middle of the list
  {
    prevP->next = nodeP->next;
  }

  nodeP->next = NULL;
}



// -----------------------------------------------------------------------------
//
// kjChildAddSorted - sorted insert of a node in a container
//
// PARAMETERS
//   container - pointer to the father (container) of the child to be added
//   child     - pointer to the child to be added
//
// NOTE
//   The `name` of the children in the container is used as criteria for the sort.
//
void kjChildAddSorted(KjNode* container, KjNode* child)
{
  //
  // First child to be added?
  //
  if (container->value.firstChildP == NULL)
  {
    container->value.firstChildP = child;
    container->lastChild         = child;
    child->next                  = NULL;

    return;
  }

  // Find where to insert the new node
  KjNode* current     = container->value.firstChildP;
  KjNode* prev        = NULL;
  char    childName0  = child->name[0];
  
  while (current != NULL)
  {
    char currentName0 = current->name[0];

    if (childName0 > currentName0)
    {
      // Still not there - must continue
      prev    = current;
      current = current->next;
      continue;
    }

    if (childName0 < currentName0)
    {
      // Got it - insert child between prev and current
      break;
    }
    
    if (strcmp(child->name, current->name) > 0)  // child->name GT current->name - must continue
    {
      // Still not there - must continue
      prev    = current;
      current = current->next;
      continue;
    }

    //
    // child->name LE current->name => insert child between prev and current
    //
    break;
  }

  //
  // Three cases:
  //
  //   01. If prev == NULL, then child is LT the containers first child and should be prepended
  //       to the list
  //
  //   02. If current == NULL, then child is to be appended to the list
  //
  //   03. Else, both prev and current points to nodes. child to be inserted between prev and current
  //
  //   Note that 'prev == NULL' and 'current == NULL' is not possible as the empty list has been
  //   taken care of already
  //
  if (prev == NULL)
  {
    child->next                  = current;
    container->value.firstChildP = child;
  }
  else if (current == NULL)
  {
    container->lastChild->next = child;
    container->lastChild       = child;
    child->next                = NULL;
  }
  else
  {
    child->next = current;
    prev->next  = child;
  }
}



// -----------------------------------------------------------------------------
//
// kjChildAddSortedReverse - reverse sorted insert of a node in a container
//
// PARAMETERS
//   container - pointer to the father (container) of the child to be added
//   child     - pointer to the child to be added
//
// NOTE
//   The `name` of the children in the container is used as criteria for the sort.
//
void kjChildAddSortedReverse(KjNode* container, KjNode* child)
{
  //
  // First child to be added?
  //
  if (container->value.firstChildP == NULL)
  {
    container->value.firstChildP   = child;
    container->lastChild           = child;
    child->next                    = NULL;

    return;
  }

  // Find where to insert the new node
  KjNode* current     = container->value.firstChildP;
  KjNode* prev        = NULL;
  char    childName0  = child->name[0];
  
  while (current != NULL)
  {
    char currentName0 = current->name[0];

    if (childName0 < currentName0)
    {
      // Still not there - must continue
      prev    = current;
      current = current->next;
      continue;
    }

    if (childName0 > currentName0)
    {
      // Got it - insert child between prev and current
      break;
    }
    
    if (strcmp(child->name, current->name) < 0)  // child->name LT current->name - must continue
    {
      // Still not there - must continue
      prev    = current;
      current = current->next;
      continue;
    }

    //
    // child->name LE current->name => insert child between prev and current
    //
    break;
  }

  //
  // Three cases:
  //
  //   01. If prev == NULL, then child is GT the containers first child and should be prepended
  //       to the list
  //
  //   02. If current == NULL, then child is to be appended to the list
  //
  //   03. Else, both prev and current points to nodes. child to be inserted between prev and current
  //
  //   Note that 'prev == NULL' and 'current == NULL' is not possible as the empty list has been
  //   taken care of already
  //
  if (prev == NULL)
  {
    child->next                  = current;
    container->value.firstChildP = child;
  }
  else if (current == NULL)
  {
    container->lastChild->next = child;
    container->lastChild       = child;
    child->next                = NULL;
  }
  else
  {
    child->next = current;
    prev->next  = child;
  }
}
