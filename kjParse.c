// 
// FILE            kjParse.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
//
// This file contains all the parsing functionality of the kjson library.
// There aren't that many functions needed:
//   - kjParseValue:        parse the value of an item
//   - kjParseMember:       parse a member of an object
//   - kjParseArrayMember:  parse a member of an array
//   - kjParseObject:       parse an object
//   - kjParseArray:        parse an array
//   - kjParse:             the entry point for json parsing
//
// Eating of whitespace is implemented as a cpp macro: EAT_WHITESPACE
//
// For the library to be thread-safe, the struct Kjson contains all the
// variables used during the json parse.
// The library contains no global vars and makes no calls to non thread-safe functions
//
// For the parse step to be fast, pure C is used and not many function calls are made.
//
// Especially malloc() is avoided by letting the users of the library supply the buffer
// to be used and a 'home-made' allocation function is used (a cpp macro actually), which
// portions out pieces of the buffer which is never freed (the recommendation is to use a
// global variable as buffer, or a thread variable if more than one invocation of kjParse
// is needed by an executable. If a thread needs to parse more than one json buffer simultaneously,
// then one separate buffer must be supplied per parse-invocation, naturally ...
//
// This initial buffer (initBuf from here on) is supplied via the call to kjInit() and the central
// Kjson buffer is created inside.
// The rest of 'initBuf' is used as initial memory buffer for the 'home made' allocations that the
// library needs during the parse step.
//
// The kjson library uses the initBuf until it is exhausted and if more memory is needed, a call
// to calloc is made, with a configurable size (see kjConfig).
//
// Allocated buffers are freed by the kjFree function, that must be called when a parsed JSON buffer
// is no longer needed.
// The library doesn't free the initBuf, as it doesn't know whether the buffer was allocated on
// the heap or if it came from the DATA segment.
// The responsibility to free the buffer lies upon the user of library.
//
// An interesting feature of the function kjFree is that it accepts a parameter 'KBool reuse', which
// lets a user reuse the initBuf for a subsequent call to parse a new jSON document.
// Of course, after a call to kjFree, the previous data is reset and can no longer be used.
//
// 0. Basic principles
//    This JSON parser aims to be the smallest and fastest on the market, so:
//      - strictly in C
//      - minimize the number of calls to malloc
//      - possibility to remove features using cpp defines
//
//    We also want it complete, so:
//      - parse/build/render JSON
//      - Support both DOM and SAX
//      - Future: KSON Pointer
//      - Future: JSON Scheme: https://en.wikipedia.org/wiki/JSON (some day)
//
//
#include <string.h>                     // strcpy, et al

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "kbase/kLibLog.h"              // K Log macros

#include "kalloc/KAlloc.h"              // KAlloc
#include "kalloc/kaAlloc.h"             // kaAlloc

#include "kjson/KjTraceLevels.h"        // Trace Levels for the kjson library
#include "kjson/kjConfig.h"             // Configuration definition
#include "kjson/KjStatus.h"             // KjStatus
#include "kjson/KjNode.h"               // KjNode
#include "kjson/kjCallbacks.h"          // KJ_SAX, KJ_ERR
#include "kjson/kjFree.h"               // kjFree
#include "kjson/kjParse.h"              // Own Interface



// -----------------------------------------------------------------------------
//
// EAT_WHITESPACE -
//
#ifdef KJ_LINE_NUMBERS
#define EAT_WHITESPACE(s)                                          \
  do                                                               \
{                                                                  \
  register char c = *s;                                            \
  while ((c == ' ') || (c == '\t') || (c == '\n') || (c == 13))    \
  {                                                                \
    if (c != '\n')                                                 \
      ++s;                                                         \
    else                                                           \
    {                                                              \
      kjP->lineNo += 1;                                            \
      ++s;                                                         \
    }                                                              \
    c = *s;                                                        \
  }                                                                \
} while (0)

#else

#define EAT_WHITESPACE(s)                                         \
do                                                                \
{                                                                 \
  register char c = *s;                                           \
  while ((c == ' ') || (c == '\t') || (c == '\n') || (c == 13))   \
  {                                                               \
    ++s;                                                          \
    c = *s;                                                       \
  }                                                               \
} while (0)
#endif



// -----------------------------------------------------------------------------
//
// isHexDigit
//
#define isHexDigit(c) (((c >= '0') && (c <= '9')) || ((c >= 'a') && (c <= 'f')) || ((c >= 'A') && (c <= 'F')))



// -----------------------------------------------------------------------------
//
// Forward declarations
//
static KjNode* kjParseObject(Kjson* kjP, KjNode* objNode IN_ARRAY_AS_PARAM);
static KjNode* kjParseArray(Kjson* kjP,  KjNode* arrNode IN_ARRAY_AS_PARAM);



// -----------------------------------------------------------------------------
//
// kjParseValue -
//
static KjStatus kjParseValue(Kjson* kjP, KjNode* nodeP IN_ARRAY_AS_PARAM)
{
  KLOG_T(KjlParseValue, "Parsing a value");
  EAT_WHITESPACE(kjP->jsonP);

  switch (*kjP->jsonP)
  {
  case ',':
    kjErrorStringSet(kjP, "JSON Parse Error: invalid value, double comma?");
    KLOG_E(0, "%s", kjP->errorString);
    KJ_ERR(kjP, 1);
    return KjsParseError;

  case '"':  // String
    KLOG_T(KjlParseValue, "Parsing a STRING value. Node (%s) at %p", nodeP->name, nodeP);

    nodeP->type    = KjString;
    nodeP->value.s = ++kjP->jsonP;
    KLOG_T(KjlParseValue, "nodeP->value.s: %s", nodeP->value.s);

    //
    // Find ending citation-mark and string-terminate the end
    //
    while (*kjP->jsonP != '"')
    {
      if (*kjP->jsonP != 0)
      {
        if (*kjP->jsonP == '\t')
        {
          kjErrorStringSet(kjP, "JSON Parse Error: tabulator in string value");
          KJ_ERR(kjP, 1);
          return KjsParseError;
        }
        else if (*kjP->jsonP == '\n')
        {
          kjErrorStringSet(kjP, "JSON Parse Error: new-line in string value");
          KJ_ERR(kjP, 1);
          return KjsParseError;
        }
        else if (*kjP->jsonP != '\\')
        {
          kjP->jsonP += 1;
        }
        else  // If backslash, step over next char
        {
          // FIXME: Duplicated code (see kjParseValue)
          kjP->jsonP += 1;

          if ((*kjP->jsonP == '"')  ||
              (*kjP->jsonP == '\\') ||
              (*kjP->jsonP == '/')  ||
              (*kjP->jsonP == 'b')  ||
              (*kjP->jsonP == 'f')  ||
              (*kjP->jsonP == 'n')  ||
              (*kjP->jsonP == 'r')  ||
              (*kjP->jsonP == 't'))
          {
            kjP->jsonP += 1;
          }
          else if (*kjP->jsonP == 'u')
          {
            char u1 = kjP->jsonP[1];
            char u2 = kjP->jsonP[2];
            char u3 = kjP->jsonP[3];
            char u4 = kjP->jsonP[4];

            if (isHexDigit(u1) && isHexDigit(u2) && isHexDigit(u3) && isHexDigit(u4))
            {
              kjP->jsonP += 4;
            }
            else
            {
              kjP->jsonP      -= 1;  // Step back to the backslash
              kjErrorStringSet(kjP, "JSON Parse Error: invalid escaped hexadecimal");
              KJ_ERR(kjP, 1);
              return KjsParseError;
            }
          }
          else
          {
            kjP->jsonP      -= 1;  // Step back to the backslash
            kjErrorStringSet(kjP, "JSON Parse Error: invalid escape sequence");
            KJ_ERR(kjP, 1);
            return KjsParseError;
          }
        }
      }
      else
      {
        kjErrorStringSet(kjP, "JSON Parse Error: no ending citation-mark found for string value");
        KLOG_E(0, "%s", kjP->errorString);
        KJ_ERR(kjP, 1);
        return KjsParseError;
      }
    }

    // Was the last citation-mark found?
    if (*kjP->jsonP != '"')
    {
      kjErrorStringSet(kjP, "JSON Parse Error: no ending citation-mark found for string value");
      KLOG_E(0, "%s", kjP->errorString);
      KJ_ERR(kjP, 1);
      return KjsParseError;
    }

    *kjP->jsonP = 0;

    ++kjP->jsonP;
    KJ_SAX(kjP, KjStringValue, nodeP->name, &nodeP->value, inArray);

    KLOG_T(KjlParseValue, "Parsed a STRING value (of length %lu) '%s' for '%s' at %p", strlen(nodeP->value.s), nodeP->value.s, nodeP->name, nodeP);
    return KjsOk;

  case '-':
  case '+':
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
#if 0
  {
    char* endP;
    nodeP->value.f = strtof(kjP->jsonP, &endP);
    kjP->jsonP = endP;
  }
#else
    //
    // Parsing a NUMBER
    //   +/- 123
    //       2.345
    //       12E54
    //       1.09E12
    //
    // ToDo:
    //   If only for presentation (json beautifier), just keep the string as is, no conversion necessary
    //

    KLOG_T(KjlParseValue, "Parsing a number: %s", kjP->jsonP);

    long long      ipart       = 0;  // integer part
    long long      fpart       = 0;  // fraction part (if float)
    unsigned int   fdiv        = 0;  // number of decimals
    long long      epart       = 0;  // exponential
    long long      expo        = 1;
    int            sign        = 1;
    KBool          isFloat     = KFALSE;

    // Sign
    if (*kjP->jsonP == '0')
    {
      // Hex, or just leading zero?
      if ((kjP->jsonP[1] == 'x') || (kjP->jsonP[1] == 'X'))
      {
        kjErrorStringSet(kjP, "JSON Parse Error: hex numbers are not admitted");
        KJ_ERR(kjP, 1);
        return KjsParseError;
      }
    }
    else if (*kjP->jsonP == '-')
    {
      sign = -1;
      ++kjP->jsonP;
    }
    else if (*kjP->jsonP == '+')
    {
      ++kjP->jsonP;
    }

    // 1. int part
    char* intStart = kjP->jsonP;

    while ((*kjP->jsonP >= '0') && (*kjP->jsonP <= '9'))
    {
      ipart = (ipart * 10) + (*kjP->jsonP - '0');
      ++kjP->jsonP;
    }

    // Leading zero?
    if ((*intStart == '0') && (kjP->jsonP > intStart + 1))
    {
      // Take input-pointer back to the first zero ...
      kjP->jsonP       = intStart;
      kjErrorStringSet(kjP, "JSON Parse Error: numbers cannot have leading zeroes");
      KJ_ERR(kjP, 1);
      return KjsParseError;
    }
    KLOG_T(KjlParseValue, "Got int-part: %lld", ipart);

    if (*kjP->jsonP == '.')
    {
      KLOG_T(KjlParseValue, "Got dot");

      isFloat = KTRUE;
      ++kjP->jsonP;

      while ((*kjP->jsonP >= '0') && (*kjP->jsonP <= '9'))
      {
        fpart = (fpart * 10) + (*kjP->jsonP - '0');
        fdiv++;
        ++kjP->jsonP;
      }
      KLOG_T(KjlParseValue, "GOT fraction part: %lld (fdiv=%d)", fpart, fdiv);
    }

    int esign = 1;
    if ((*kjP->jsonP == 'E') || (*kjP->jsonP == 'e'))
    {
      KLOG_T(KjlParseValue, "GOT E/e");
      ++kjP->jsonP;
      if (*kjP->jsonP == '-')
      {
        esign = -1;
        ++kjP->jsonP;
      }
      else if (*kjP->jsonP == '+')
      {
        ++kjP->jsonP;
      }

      char* eStart = kjP->jsonP;
      while ((*kjP->jsonP >= '0') && (*kjP->jsonP <= '9'))
      {
        epart = (epart * 10) + (*kjP->jsonP - '0');
        ++kjP->jsonP;
      }
      if (eStart == kjP->jsonP)
      {
        kjErrorStringSet(kjP, "JSON Parse Error: invalid number - missing exponent");
        KJ_ERR(kjP, 1);
        return KjsParseError;
      }
    }

#ifdef USE_VALUE_STRING
    char* numberStart = kjP->jsonP;
    if (kjP->numbersAsStrings == KTRUE)
    {
      int size = kjP->jsonP - numberStart;

      nodeP->valueString = kaAlloc(kjP->kallocP, size + 1);
      nodeP->value.s     = nodeP->valueString;

      strncpy(nodeP->valueString, numberStart, size);
      nodeP->valueString[size] = 0;
      KLOG_T(KjlParseValue, "numbersAsStrings: valueString for '%s': '%s'\n", nodeP->name, nodeP->valueString);

      if (isFloat == KTRUE)
      {
        nodeP->type = KjFloat;
        KJ_SAX(kjP, KjFloatValue, nodeP->name, &nodeP->value, inArray);
      }
      else
      {
        nodeP->type = KjInt;
        KJ_SAX(kjP, KjIntegerValue, nodeP->name, &nodeP->value, inArray);
      }
    }
    else
#endif
    {
      if (epart != 0)
      {
        KLOG_T(KjlParseValue, "epart=%d, expo=%d", epart, expo);
        while (epart > 5)
        {
          expo *= 100000;
          epart -= 5;
        }

        while (epart-- > 0)
        {
          expo *= 10;
        }
      }

      if (isFloat == KTRUE)
      {
#if 1
        double f;

        KLOG_T(KjlParseValue, "Parsing a FLOAT (numbersAsStrings == false)");

        f = fpart;
#if 0
        while (fdiv >= 6)
        {
          f = f / 1000000;
          fdiv -= 6;
        }
        
        while (fdiv >= 3)
        {
          f = f / 1000;
          fdiv -= 3;
        }

        while (fdiv >= 1)
        {
          f = f / 10;
          --fdiv;
        }
#else
        while (fdiv > 0)
        {
          switch (fdiv)
          {
          case 0: break;
          case 1:  f = f / 10;           fdiv = 0; break;
          case 2:  f = f / 100;          fdiv = 0; break;
          case 3:  f = f / 1000;         fdiv = 0; break;
          case 4:  f = f / 10000;        fdiv = 0; break;
          case 5:  f = f / 100000;       fdiv = 0; break;
          case 6:  f = f / 1000000;      fdiv = 0; break;
          case 7:  f = f / 10000000;     fdiv = 0; break;
          case 8:  f = f / 100000000;    fdiv = 0; break;
          case 9:  f = f / 1000000000;   fdiv = 0; break;
          case 10: f = f / 10000000000;  fdiv = 0; break;
          default: f = f / 100000000000; fdiv -= 11; break;
          }
        }
#endif
        nodeP->type = KjFloat;

        if (esign == 1)
          nodeP->value.f  = sign * (ipart + f) * expo;
        else
          nodeP->value.f  = (sign * (ipart + f)) / expo;

        KLOG_T(KjlParseValue, "Parsed a FLOAT value '%f' for '%s' at %p", nodeP->value.f, nodeP->name, nodeP);
        KJ_SAX(kjP, KjFloatValue, nodeP->name, &nodeP->value, inArray);
#endif
      }
      else
      {
        nodeP->type     = KjInt;
        nodeP->value.i  = sign * ipart * expo;
        KLOG_T(KjlParseValue, "Parsed an INTEGER value '%lld' for '%s' at %p", nodeP->value.i, nodeP->name, nodeP);
        KJ_SAX(kjP, KjIntegerValue, nodeP->name, &nodeP->value, inArray);
      }
    }
#endif
    return KjsOk;

  case '{':  // JSON Object
    nodeP->type = KjObject;
    KLOG_T(KjlParseValue, "Parsing an OBJECT for '%s' at %p", nodeP->name, nodeP);
    ++kjP->jsonP;

    KJ_SAX(kjP, KjObjectStart, nodeP->name, NULL, inArray);
    if (kjParseObject(kjP, nodeP IN_ARRAY) != NULL)
      return KjsOk;

    // kjP->errorString set by kjParseObject()
    return KjsParseError;

  case '[':  // JSON Array
    KLOG_T(KjlParseValue, "Parsing an ARRAY as value for '%s' at %p", nodeP->name, nodeP);
    nodeP->type         = KjArray;
    ++kjP->jsonP;

    KJ_SAX(kjP, KjArrayStart, nodeP->name, NULL, inArray);
    if (kjParseArray(kjP, nodeP IN_ARRAY) != NULL)
      return KjsOk;

    // kjP->errorString set by kjParseArray()
    KLOG_E("%s", kjP->errorString);
    KJ_ERR(kjP, 1);
    return KjsParseError;

  case 'n':  // Possible 'null'
    if ((kjP->jsonP[1] == 'u') && (kjP->jsonP[2] == 'l') && (kjP->jsonP[3] == 'l'))
    {
      KLOG_T(KjlParseValue, "Parsed a NULL value for '%s' at %p",  nodeP->name, nodeP);
      nodeP->type = KjNull;
      kjP->jsonP += 4;

      KJ_SAX(kjP, KjNullValue, nodeP->name, NULL, inArray);
      return KjsOk;
    }

    kjErrorStringSet(kjP, "JSON Parse Error: invalid value");
    KLOG_E("%s", kjP->errorString);
    KJ_ERR(kjP, 1);
    return KjsParseError;

  case 't':  // Possible true
    if ((kjP->jsonP[1] == 'r') && (kjP->jsonP[2] == 'u') && (kjP->jsonP[3] == 'e'))
    {
      KLOG_T(KjlParseValue, "Parsed a TRUE value for '%s' at %p",  nodeP->name, nodeP);
      nodeP->type    = KjBoolean;
      nodeP->value.b = KTRUE;
      kjP->jsonP += 4;

      KJ_SAX(kjP, KjBoolValue, nodeP->name, &nodeP->value, inArray);
      return KjsOk;
    }

    kjErrorStringSet(kjP, "JSON Parse Error: invalid value");
    KLOG_E("%s", kjP->errorString);
    KJ_ERR(kjP, 1);
    return KjsParseError;

  case 'f':  // Possible false
    if ((kjP->jsonP[1] == 'a') && (kjP->jsonP[2] == 'l') && (kjP->jsonP[3] == 's') && (kjP->jsonP[4] == 'e'))
    {
      nodeP->type    = KjBoolean;
      nodeP->value.b = KFALSE;
      kjP->jsonP += 5;
      KLOG_T(KjlParseValue, "Parsed a FALSE value for '%s' at %p",  nodeP->name, nodeP);

      KJ_SAX(kjP, KjBoolValue, nodeP->name, &nodeP->value, inArray);
      return KjsOk;
    }

    kjErrorStringSet(kjP, "JSON Parse Error: invalid value");
    KLOG_E("%s", kjP->errorString);
    KJ_ERR(kjP, 1);
    return KjsParseError;

  default:
    kjErrorStringSet(kjP, "JSON Parse Error: invalid value");
    KLOG_E("%s (%c)", kjP->errorString, *kjP->jsonP);
    KJ_ERR(kjP, 1);
    return KjsParseError;
  }

  KLOG_E(0, "PARSE ERROR");
  KJ_ERR(kjP, 1);
  return KjsParseError;
}



// -----------------------------------------------------------------------------
//
// kjErrorString -
//
void kjErrorStringSet(Kjson* kjP, const char* errorText)
{
  if (errorText == NULL)
  {
    kjP->errorString[0] = 0;
    return;
  }

  strncpy(kjP->errorString, errorText, sizeof(kjP->errorString));
}



// -----------------------------------------------------------------------------
//
// kjParseMember -
//
static KjNode* kjParseMember(Kjson* kjP, KjNode* objectP)
{
  EAT_WHITESPACE(kjP->jsonP);

  KLOG_T(KjlParseObject, "parsing a member: %s", kjP->jsonP);
  if (*kjP->jsonP != '"')
  {
    kjErrorStringSet(kjP, "JSON Parse Error: no starting citation-mark found for name of member");
    KJ_ERR(kjP, 1);
    return NULL;
  }

  KLOG_T(KjlParseObject, "Step over citation-mark and save as start of name of member");
  // Step over citation-mark and save as start of name of member
  char* nameStart = ++kjP->jsonP;

  // Valid chars inside node name?
  while ((*kjP->jsonP != '"') && (*kjP->jsonP != 0))
  {
    if (*kjP->jsonP == '\t')
    {
      kjErrorStringSet(kjP, "JSON Parse Error: tabulator in member name");
      KJ_ERR(kjP, 1);
      return NULL;
    }
    else if (*kjP->jsonP == '\n')
    {
      kjErrorStringSet(kjP, "JSON Parse Error: new-line in member name");
      KJ_ERR(kjP, 1);
      return NULL;
    }
    else if (*kjP->jsonP != '\\')
    {
      kjP->jsonP += 1;
    }
    else  // If backslash, step over next char
    {
      //
      // Valid escaped chars/strings :
      //   \"
      //   \\  (escaped backslash)
      //   \/
      //   \b
      //   \f
      //   \n
      //   \r
      //   \t
      //   \u four-hex-digits
      //
      kjP->jsonP += 1;  // Step over the backslash
      KLOG_T(KjlParseObject, "Got a backslash. Next char is %c", *kjP->jsonP);
      if ((*kjP->jsonP == '"')  ||
          (*kjP->jsonP == '\\') ||
          (*kjP->jsonP == '/')  ||
          (*kjP->jsonP == 'b')  ||
          (*kjP->jsonP == 'f')  ||
          (*kjP->jsonP == 'n')  ||
          (*kjP->jsonP == 'r')  ||
          (*kjP->jsonP == 't'))
      {
        kjP->jsonP += 1;
      }
      else if (*kjP->jsonP == 'u')
      {
        char u1 = kjP->jsonP[1];
        char u2 = kjP->jsonP[2];
        char u3 = kjP->jsonP[3];
        char u4 = kjP->jsonP[4];

        if (isHexDigit(u1) && isHexDigit(u2) && isHexDigit(u3) && isHexDigit(u4))
        {
          kjP->jsonP += 4;
        }
        else
        {
          kjP->jsonP      -= 1;  // Step back to the backslash
          kjErrorStringSet(kjP, "JSON Parse Error: invalid escaped hexadecimal");
          KJ_ERR(kjP, 1);
          return NULL;
        }
      }
      else
      {
        kjP->jsonP      -= 1;  // Step back to the backslash
        kjErrorStringSet(kjP, "JSON Parse Error: invalid escape sequence");
        KJ_ERR(kjP, 1);
        return NULL;
      }
    }
  }

  if (*kjP->jsonP == 0)
  {
    kjErrorStringSet(kjP, "JSON Parse Error: no ending citation-mark found for name of member");
    return NULL;
  }

  // Zero out the citation-mark
  *kjP->jsonP = 0;
  ++kjP->jsonP;

  // We have a name ...
  KjNode* nodeP;

#ifndef KJ_DOM_ON
  KjNode node;
  nodeP = &node;
  nodeP->name = nameStart;
#else
  {
    nodeP = (KjNode*) kaAlloc(kjP->kallocP, sizeof(KjNode));
    // bzero(nodeP, sizeof(KjNode));  // FIXME: seems not necessary ...
    nodeP->name = nameStart;
  }
#endif

  KLOG_T(KjlParseObject, "Setting nodeP->next to NULL (node name: %s)", nodeP->name);
  nodeP->next = NULL;

  //
  // Add node to its container (objectP)
  // If 'unsorted insert', perform an inline append - all items
  // in the order they were read
  //
#ifdef KJ_DOM_ON
  if (kjP->addF == NULL)  // FIXME: make addF point to kjChildAdd and stop doing "if (kjP->addF == NULL)"
  {
    if (objectP->value.firstChildP != NULL)
    {
      objectP->lastChild->next = nodeP;
    }
    else
      objectP->value.firstChildP = nodeP;

    // new child is the last child
    objectP->lastChild = nodeP;
  }
  else
    kjP->addF(objectP, nodeP);
#endif

  // Now a colon MUST come
  EAT_WHITESPACE(kjP->jsonP);

  if (*kjP->jsonP != ':')
  {
    kjErrorStringSet(kjP, "JSON Parse Error: no colon found after name of member");
    KLOG_E(kjP->errorString);
    KJ_ERR(kjP, 1);
    return NULL;
  }
  ++kjP->jsonP;

  // Now, the value

  KLOG_T(KjlParseObject, "kjParseMember calls kjParseValue");
  if (kjParseValue(kjP, nodeP IN_ARRAY_FALSE) != KjsOk)
  {
    // kjP->errorString set by kjParseValue()
    KLOG_E(kjP->errorString);
    KJ_ERR(kjP, 1);
    return NULL;
  }

  return nodeP;
}



// -----------------------------------------------------------------------------
//
// kjParseArrayMember -
//
static KjNode* kjParseArrayMember(Kjson* kjP, KjNode* arrayP)
{
  EAT_WHITESPACE(kjP->jsonP);

  // NOT Empty array?
  if (*kjP->jsonP != ']')
  {
    KjNode* nodeP;

#ifndef KJ_DOM_ON
    KjNode node;
    nodeP = &node;
#else
    nodeP = (KjNode*) kaAlloc(kjP->kallocP, sizeof(KjNode));
    bzero(nodeP, sizeof(KjNode));
#endif

#ifdef KJ_DOM_ON
    // Add node to its container (arrayP)
    if (arrayP->value.firstChildP != NULL)
      arrayP->lastChild->next = nodeP;
    else
      arrayP->value.firstChildP = nodeP;

    // Point to the last child
    arrayP->lastChild = nodeP;
#endif

    KjStatus s;
    KLOG_T(KjlParseObject, "kjParseArrayMember calling kjParseValue");
    if ((s = kjParseValue(kjP, nodeP IN_ARRAY_TRUE)) != KjsOk)
    {
      KLOG_E("kjParseValue returned %s for node '%s'", kjStatus(s), nodeP->name);
      KJ_ERR(kjP, 1);
      return NULL;
    }

#ifdef KJ_LOG_ON
    char v[64];
    KLOG_T(KjlParseObject, "Parsed an ARRAY-member: '%s'", kjValue(nodeP, v, 64));
#endif
    return nodeP;
  }

  ++kjP->jsonP;

  return arrayP;
}



// -----------------------------------------------------------------------------
//
// kjParseObject -
//
static KjNode* kjParseObject(Kjson* kjP, KjNode* objNode IN_ARRAY_AS_PARAM)
{
  KLOG_T(KjlParseObject, "Parsing an OBJECT: %s", kjP->jsonP);

  objNode->type = KjObject;

  // Eat preceding whitespace
  EAT_WHITESPACE(kjP->jsonP);

  // Empty object?
  if (*kjP->jsonP == '}')
  {
    KLOG_T(KjlParseObject, "Got an EMPTY OBJECT");
    ++kjP->jsonP;

    KJ_SAX(kjP, KjObjectEnd, objNode->name, NULL, inArray);
    return objNode;
  }

  while (1)
  {
    KLOG_T(KjlParseObject, "Parsing object-member '%s': %s", objNode->name, kjP->jsonP);
    kjErrorStringSet(kjP, NULL);
    if (kjParseMember(kjP, objNode) == NULL)
    {
      KJ_ERR(kjP, 1);
      KLOG_E("kjParseMember returned NULL. Pos %d: '%s'", kjP->errorPos, kjP->jsonP);
      return NULL;
    }

    // Eat whitespace
    EAT_WHITESPACE(kjP->jsonP);

    if ((*kjP->jsonP != ',') && (*kjP->jsonP != '}'))
    {
      if (kjP->errorString[0] == 0)
        kjErrorStringSet(kjP, "JSON Parse Error: expecting comma or end of object");
      KJ_ERR(kjP, 1);
      KLOG_E("Expected comma or end of object. Pos %d: '%s'", kjP->errorPos, kjP->jsonP);
      return NULL;
    }

    if (*kjP->jsonP == '}')
    {
      ++kjP->jsonP;
      KJ_SAX(kjP, KjObjectEnd, objNode->name, NULL, inArray);
      break;
    }

    ++kjP->jsonP;  // Eating the comma, and continuing ...
  }

  return objNode;
}



// -----------------------------------------------------------------------------
//
// kjParseArray -
//
static KjNode* kjParseArray(Kjson* kjP, KjNode* arrNode IN_ARRAY_AS_PARAM)
{
  KLOG_T(KjlParseArray, "Parsing an ARRAY: %s", kjP->jsonP);

  arrNode->type = KjArray;

  // Eat preceding whitespace
  EAT_WHITESPACE(kjP->jsonP);

  // Empty array?
  if (*kjP->jsonP == ']')
  {
    ++kjP->jsonP;
    KJ_SAX(kjP, KjArrayEnd, arrNode->name, NULL, inArray);
    return arrNode;
  }

  while (1)
  {
    if (*kjP->jsonP == ']')
    {
      ++kjP->jsonP;
      KJ_SAX(kjP, KjArrayEnd, arrNode->name, NULL, inArray);
      return arrNode;
    }

    if (kjParseArrayMember(kjP, arrNode) == NULL)
    {
      KLOG_T(KjlParseArray, "Error parsing ArrayMember");
      KJ_ERR(kjP, 1);
      return NULL;
    }

    // Eat whitespace
    EAT_WHITESPACE(kjP->jsonP);


    if ((*kjP->jsonP != ',') && (*kjP->jsonP != ']'))
    {
      kjErrorStringSet(kjP, "JSON Parse Error: expecting comma or end of array");
      KLOG_E("%s. Got %c", kjP->errorString, *kjP->jsonP);
      KJ_ERR(kjP, 1);

      return NULL;
    }

    if (*kjP->jsonP == ']')
    {
      ++kjP->jsonP;
      KJ_SAX(kjP, KjArrayEnd, arrNode->name, NULL, inArray);
      break;
    }

    ++kjP->jsonP;  // Eating the comma, and continuing ...
    int commaLine = kjP->lineNo;

    EAT_WHITESPACE(kjP->jsonP);

    if (*kjP->jsonP == ']')
    {
      kjP->lineNo      = commaLine;
      kjErrorStringSet(kjP, "JSON Parse Error: trailing comma");
      KLOG_E("%s", kjP->errorString);
      KJ_ERR(kjP, 1);

      return NULL;
    }
  }

  return arrNode;
}



// -----------------------------------------------------------------------------
//
// kjParse -
//
KjNode* kjParse(Kjson* kjP, char* json)
{
  KLOG_T(KjlParse, "Parsing '%s'", json);

  if ((json == NULL) || (*json == 0))
    KLOG_RE(NULL, "no json buffer");

  KjNode*  top = (KjNode*) kaAlloc(kjP->kallocP, sizeof(KjNode));
  bzero(top, sizeof(KjNode));

  if (top == NULL)
    KLOG_RE(NULL, "calloc");

#ifdef KJ_DOM_ON
  kjP->tree              = top;
  top->next              = NULL;
  top->value.firstChildP = NULL;
  top->lastChild         = NULL;
#endif

  kjP->json  = json;
  kjP->jsonP = json;

  EAT_WHITESPACE(kjP->jsonP);

  KJ_SAX(kjP, KjSaxStart, NULL, NULL, KFALSE);

  if (*kjP->jsonP == '{')
  {
    top->name = "toplevel object";  // Will not be freed

    ++kjP->jsonP;
    KJ_SAX(kjP, KjObjectStart, top->name, NULL, KFALSE);

    if (kjParseObject(kjP, top IN_ARRAY_FALSE) == NULL)
    {
      KLOG_E("kjParseObject returned NULL");
      top = NULL;  // mark as erroneous
    }
  }
  else if (*kjP->jsonP == '[')
  {
    top->name = "toplevel array";  // Will not be freed

    ++kjP->jsonP;
    KJ_SAX(kjP, KjArrayStart, top->name, NULL, KFALSE);

    if (kjParseArray(kjP, top IN_ARRAY_FALSE) == NULL)
    {
      KLOG_E("kjParseObject returned NULL");
      top = NULL;  // mark as erroneous
    }
  }
  else
  {
    top->name = "toplevel value";

    if (kjParseValue(kjP, top IN_ARRAY_FALSE) != KjsOk)
    {
      KLOG_E("kjParseValue error");
      top = NULL;  // mark as erroneous
    }
  }

  //
  // If erroneous, offer error callbacks and return NULL
  //
  if (top == NULL)
  {
    KJ_ERR(kjP, 1);
    return NULL;
  }

  //
  // Trailing garbage?
  //
  EAT_WHITESPACE(kjP->jsonP);
  if (*kjP->jsonP != 0)
  {
    kjErrorStringSet(kjP, "garbage after document end: ");
    sprintf(&kjP->errorString[28], "'%c'", *kjP->jsonP);
    KJ_ERR(kjP, 1);
    return NULL;
  }

  KJ_SAX(kjP, KjSaxEnd, NULL, NULL, KFALSE);

  return top;
}
