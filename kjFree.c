// 
// FILE            kjFree.c - free allocated mem after JSON parse is done
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <string.h>                     // memset
#include <stdlib.h>                     // free

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "kbase/kLibLog.h"              // K Log macros

#include "kjson/KjNode.h"               // KjNode
#include "kjson/KjTraceLevels.h"        // KjlFree
#include "kjson/kjFree.h"               // Own Interface



// -----------------------------------------------------------------------------
//
// kjFree
//
void kjFree(KjNode* kNodeP)
{
  if (kNodeP->name != NULL)
    KLOG_T(KjlFree, "Freeing %s KjNode (%s) at %p", kjValueType(kNodeP->type), kNodeP->name, kNodeP);
  else
    KLOG_T(KjlFree, "Freeing %s KjNode at %p", kjValueType(kNodeP->type), kNodeP);

  if ((kNodeP->type == KjArray) || (kNodeP->type == KjObject))
  {
    KjNode* nodeP = kNodeP->value.firstChildP;

    while (nodeP != NULL)
    {
      KjNode* next = nodeP->next;

      kjFree(nodeP);
      nodeP = next;
    }
  }
#if 0
  if (kNodeP->type == KjString)
    free(kNodeP->value.s);  // Seems to give problems sometimes with "Invalid free()" ...
#endif

  // Remember that the name of the node (kNodeP->name) is allocated together with the kNodeP itself. Only one free needed
  free(kNodeP);
}
