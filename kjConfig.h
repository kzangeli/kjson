// 
// FILE            kjConfig.h - configure the behaviour of kjson
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJ_CONFIG_H_
#define KJSON_KJ_CONFIG_H_

#include "kjson/kjson.h"                // Kjson struct
#include "kjson/KjStatus.h"             // KjStatus



// -----------------------------------------------------------------------------
//
// Configuration as a set of cpp defines
//
#define KJ_SAX_ON       1
#define KJ_DOM_ON       1
#define KJ_LINE_NUMBERS 1



// -----------------------------------------------------------------------------
//
// KjConfigItem - 
//
//   KjcErrorFunction
//   KjcSaxFunction
//   KjcIndentStep
//   KjcNewlineString
//   KjcDefault
//   KjcSpaceBeforeColon
//   KjcNoSpaceAfterColon
//   KjcObjectStartOnNewLine
//   KjcArrayStartOnNewLine
//   KjcArraysOnOneLine
//   KjcShortArraysOnOneLine
//   KjcObjectsOnOneLine
//   KjcShortObjectsOnOneLine
//   KjcMinimized
//   KjcNumbersAsStrings
//   KjcUnsorted
//   KjcSorted
//   KjcSortedReverse
//   KjcShortArrayMaxLen
//   KjcShortObjectMaxLen
//
typedef enum KjConfigItem
{
  KjcSaxFunction,
  KjcErrorFunction,
  KjcIndentStep,
  KjcNewlineString,
  KjcDefault,
  KjcSpaceBeforeColon,
  KjcNoSpaceAfterColon,
  KjcObjectStartOnNewLine,
  KjcArrayStartOnNewLine,
  KjcArraysOnOneLine,
  KjcShortArraysOnOneLine,
  KjcObjectsOnOneLine,
  KjcShortObjectsOnOneLine,
  KjcMinimized,
  KjcNumbersAsStrings,
  KjcUnsorted,
  KjcSorted,
  KjcSortedReverse,
  KjcShortArrayMaxLen,
  KjcShortObjectMaxLen
} KjConfigItem;



// -----------------------------------------------------------------------------
//
// kjConfig
//
extern KjStatus kjConfig(Kjson* kjP, KjConfigItem item, char* value);

#endif  // KJSON_KJ_CONFIG_H_
