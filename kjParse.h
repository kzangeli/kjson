// 
// FILE            kjParse.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJ_PARSE_H
#define KJ_PARSE_H

#include "kjson/KjNode.h"      // KjNode
#include "kjson/kjson.h"       // Kjson struct



// -----------------------------------------------------------------------------
//
// kjParse - 
//
extern KjNode* kjParse(Kjson* kjP, char* json);



// -----------------------------------------------------------------------------
//
// kjErrorStringSet -
//
extern void kjErrorStringSet(Kjson* kjP, const char* errorText);

#endif
