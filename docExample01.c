// 
// FILE            docExample01.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdlib.h>            // exit
#include <stdio.h>             // printf
#include <string.h>            // strdup

#include "kjson/kjson.h"
#include "kjson/kjConfig.h"
#include "kjson/kjParse.h"
#include "kjson/kjBufferCreate.h"



// -----------------------------------------------------------------------------
//
// main - docExample01 - simple test program of libkjson
//
int main(int argC, char* argV[])
{
  // char*   jsonBuffer = strdup("{ \"obj\": {}, \"vec\": [], \"i\": 12, \"f\": 3.14, \"T\": true, \"F\": false, \"N\": null }");
  char*    jsonBuffer = strdup("falseh");
  Kjson*   kjP        = NULL;
  KjNode*  top;

  if ((top = kjParse(kjP, jsonBuffer)) == NULL)
  {
    fprintf(stderr, "%s[line %d]: %s (in buf pos %d)\n",
            "jsonBuffer", kjP->lineNo, kjP->errorString, kjP->errorPos);
    exit(1);
  }

  //
  // As an example, iterate over the members of the top node and print their types
  // Note that 'top' could be either an array or an object. The iteration of the members
  // is done the same way either way.
  //
  // [ and if a leaf, top->children would be NULL and we wouldn't enter the loop ]
  //
  KjNode* nodeP = top->value.firstChildP;
  int     cIx   = 0;
    
  printf("the top node is an %s\n", (top->type == KjArray)? "array" : "object");
  while (nodeP != NULL)
  {
    printf("  Member %d is of type '%s'\n", cIx, kjValueType(nodeP->type));
    nodeP = nodeP->next;
    ++cIx;
  }

  return 0;
}
