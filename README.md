# kjson - simple library for parsing, building and rendering JSON


## Background
While working on the Orion Context Broker, the library for parsing JSON that we used was rapidjson.
I never liked rapidjson as I found it far too difficult to use and I asked by bosses again and again to
let me implement my own JSON parser. Never got the OK.
So, some time later I started to implement my JSON parser in my spare time, and I'm quite satisfied with the result.
`kjson` is *very* easy to use and it is pretty fast.
Quite faster than `rapidjson` on pretty much all types of inputs except big arrays of floats.
And rapidjson is said to be the fastest JSON parse in the world so ....
I must say I'm impressed with the speed of rapidjson, not that it's faster than `kjson` but, even though it's implemented in C++
it is not that much slower than `kjson`, which is written in C. That's impressive.
On the other hand, I can't understand why anyone would want to implement a library such as this one in C++ ...


## Install
% make di  # debug install


## Usage
### Parsing
The kjson library uses the approach 'init-config-run'. Personally I have always preferred this approach
as the users of the library are spared functions with too many parameters.
A typical usage of libkjson to parse a JSON buffer could look like this:

docExample01.c:
```
    01:  #include <stdlib.h>            // exit
    02:  #include <stdio.h>             // printf
    03:  #include <string.h>            // strdup
    04:
    05:  #include "kjson/kjson.h"
    06:
    07:  // -----------------------------------------------------------------------------
    08:  //
    09:  // main - docExample01 - simple test program of libkjson
    10:  //
    11:  int main(int argC, char* argV[])
    12:  {
    13:    char*   jsonBuffer = strdup("{ \"obj\": {}, \"vec\": [], \"i\": 12, \"f\": 3.14, \"T\": true, \"F\": false, \"N\": null }");
    14:    char    parseBuffer[1024];
    15:    Kjson*  kjP;
    16:    KjNode* top;
    17:
    18:    kjP = kjInit(parseBuffer, sizeof(parseBuffer), KJ_FALSE);
    19:
    20:    kjConfig(kjP, KjcMinimized, NULL);
    21:    // (more calls to 'kjConfig' if desired)
    22:
    23:    if ((top = kjParse(kjP, jsonBuffer)) == NULL)
    24:    {
    25:      fprintf(stderr, "%s[line %d]: %s (in buf pos %d)\n",
    26:              "jsonBuffer", kjP->lineNo, kjP->errorString, kjP->errorPos);
    27:      exit(1);
    28:    }
    29:
    30:  //
    31:  // As an example, iterate over the members of the top node and print their types
    32:  // Note that 'top' could be either an array or an object. The iteration of the members
    33:  // is done the same way either way.
    34:  //
    35:  // [ and if a leaf, top->children would be NULL and we wouldn't enter the loop ]
    36:  //
    37:  KjNode* nodeP = top->children;
    38:  int     cIx   = 0;
    39:
    40:  printf("the top node is an %s\n", (top->type == KjArray)? "array" : "object");
    41:  while (nodeP != NULL)
    42:  {
    43:    printf("  Member %d is of type '%s'\n", cIx, kjValueType(nodeP->type));
    44:    nodeP = nodeP->next;
    45:    ++cIx;
    46:  }
    47:
    48:  return 0;
    49: }
```

Compile (this source file 'docExample01.c' is found in kjson's git repo):
```
    % make
```
[_Note that the kjson library depends on three "K-libraries": kalloc, klog and kbase. See the makefile._]

And run:
```
    % ./docExample01
```
Output:
```
      the top node is an object
      Member 0 is of type 'Object'
      Member 1 is of type 'Array'
      Member 2 is of type 'Int'
      Member 3 is of type 'Float'
      Member 4 is of type 'Bool'
      Member 5 is of type 'Bool'
      Member 6 is of type 'Null'
```
Play with the variable 'jsonBuffer' if you like, to see different outputs and perhaps errors.
As an example: 'jsonBuffer = strdup("falseh")' gives this output:

```
    jsonBuffer[line 1]: garbage after document end: 'h' (in buf pos 6)
```
Note that instead of editing the source file and recompiling, you can use the tool 'kjson'
to play around with:

Either using a pipe:
```
      % echo falseh | kjson
      kjson: pipe[1]: garbage after document end: 'h' (offending buffer position: 6)
```
OR, using a file:
```
      % echo falseh > /tmp/falseh
      % kjson /tmp/falseh
      kjson: /tmp/falseh[1]: garbage after document end: 'h' (offending buffer position: 6)
      % \rm /tmp/falseh
```
The kjson tool is described in detail later (FIXME!).
Right now we should spend some time looking into some details of the test program (docExample01.c).

- Line 05: First of all, the header file 'kjson.h' contains what most people need, no more includes needed.

- Line 13: jsonBuffer is strdupped. If you try to compile the test program without strdup, just assigning the
           string to jsonBuffer (in read-only segement), you will get a Segmentation Fault.
           This is because libkjson modifies the incoming string buffer, to save valuable time. For example,
           all names of object members aren't copied, just pointed to, inside the input JSON buffer. For this
           to work, the ending double-quote must of corse be replaced b y a ZERO, to end the string.
           And, this doesn't work unless the incoming buffer is writable.

- Line 14: parseBuffer - the buffer that libkjson uses for 'internal mallocs', to reserve space for nodes in the
           tree that is created during parse.
           This is another trick to speed libkjson up a little (not just a little - calls to malloc() are expensive)
           If this buffer is really rally small, so that not even a Kjson object fits, then en error is returned.
           What is left after 'allocating' the Kjson object is used to create nodes during the parse step.
           When running out of memory in the buffer, malloc is called. Not to allocate just the next node in the tree,
           but to allocate a new buffer for internal allocations. Default size is 2 megabytes, which is configurable
           using this call: 'kjConfig(kjP, KjcAllocBufSize, (char*) 16 * 1024)' - if you want mallos calls of 16kb only.
           The bigger the buffer, the faster your parser will be.

- Line 15: The structure Kjson contains all the information that libkjson needs to parse, render etc a json document.
           All configuration is stored inside Kjson, as well as the resulting tree after parse.
           The buffer is allocated and returned by the function kjInit().

- Line 18: kjInit simple retuirns a pointer to a Kjson object, allocated inside 'parseBuffer'.
           The third parameter to kjInit tells the function whether it needs to zero 'parseBuffer' or not.
           To avoid adding zeroes no node contents, the parseBuffer must contain all zero values.
           This is also to gain some time during parse.

- Line 20: The call to kjConfig tells libkjson to output a minimized rendered output (if kjRender() is called).
           kjConfig does plenty of configuration, documented in kjConfig.h

- Line 23: kjParse parses the JSON document in 'jsonBuffer' and returns a pointer to the top node of the resulting tree.
           In case of parse error, the variable 'kjP' contains a few useful fields to understand the error:
             - int lineNo           - Contains the line number of the erroneous line
             - int errorPos         - Contains the position in the buffer where the error was detected
             - char* errorString    - Points to a description of the error

- Line 25: Here you see a normal usage of the error info fields just mentioned.

- Line 37: The members of an array or object are found in the field 'children'.
           Remember that 'top' is the top node of the tree, returned by kjParse (but also stored inside the Kjson struct,
           in the field 'Kjson::tree').
           This is a simple single linked list. just follow the next-pointer and you get the following sibling.

Not much more to say about this simple test program.
Except perhaps, it doesn't get more difficult than this.
More configuration params, as much as you want, but ... as that's just one simple call to kjConfig, it doesn't really add any complexity.

### Building KjNode trees
The kjson library also offers to build your own JSON trees, that you later probably will want to render to a string.
What better than a simple example to show how it's done?

docExample02.c:
```
    01: #include <stdio.h>             // printf
    02:
    03: #include "kjson/kjson.h"       // kjson lib header
    04: #include "kjson/kjBuilder.h"   // kjson builder header
    05:
    06: static char allocBuffer[2 * 1024 + 1024];
    07:
    08: // -----------------------------------------------------------------------------
    09: //
    10: // main - docExample02 - simple test program of libkjson
    11: //
    12: int main(int argC, char* argV[])
    13: {
    14:   Kjson*  kjP  = kjInit(allocBuffer, sizeof(allocBuffer), KJ_FALSE);
    15:   KjNode* top  = kjObject(kjP, "top");
    16:   KjNode* a    = kjArray(kjP, "a");
    17:   KjNode* s1   = kjString(kjP, "s", "a string");
    18:   KjNode* i1   = kjInteger(kjP, "fourteen", 14);
    19:
    20:   kjChildAdd(top, a);
    21:   kjChildAdd(a, s1);
    22:   kjChildAdd(a, i1);
    23:
    24:   // Render the tree
    25:   char rBuf[1024];
    26:   kjRender(kjP, top, rBuf, sizeof(rBuf));
    27:   printf("%s\n", rBuf);
    28:
    29:   return 0;
    30: }
```
Pretty simple, just create containers (array or object), and leafs (string, integer, float, bool, null) and add leafs to
containers.

Not sure any further explanation is needed, but, here goes ...

- Line 04: Another header file needed for building your own json trees: kjson/kjBuilder.h.

- Line 06: 'allocBuffer' is the buffer used by libkjson to allocate memory for the nodes

- Line 14: Initialize the kjson lib, giving it allocBuffer

- Line 15: Create an Object - meant as the top node of the tree (could also be an array, or even a leaf)

- Line 16: Create an array, later to be set as a mamber of 'top' (on line 20).

- Line 17: Create a string, named 's' and with value 'a string'.
           Note that 's1' is later inserted in the array 'a', so the name of s1 will never be used.
           This is the price to pay to have arrays and object treated the same way. A small price IMHO.

- Line 18: Create a integer, named 'fourteen', with tyhe value 14.
           As on line 17, the name 'fourteen' isn't used - send in NULL as name if you like :-)

- Line 20: Add the array 'a' to the tree toplevel node - the object 'top'.

- Line 21: Add the string 's1' to the array 'a'.

- Line 22: Add the integer 'i1' to the array 'a'.

- Line 25: Important. The function kjRender renders the stringified JSON output and for that, a buffer is needed.
           This buffer is sent to kjRender to hold the stringified output. If it is not big enough, an error is
           returned by kjRender.

- Line 26: kjRender is the function that takes care of stringifying the JSON tree.

Again, pretty simple.
What's missing in this example is the numerous ways of configuring how we want kjRender to behave.
The section 'Rendering' explains all of it in detail.


### Rendering
```
void kjRender(Kjson* kjP, KjNode* nodeP, char* buf, int bufLen);
```
Use `kjConfig` before calling `kjRender` to set the characteristics on how you want the rendering:
```
KjStatus kjConfig(Kjson* kjP, KjConfigItem item, char* value);
```
The "config items" for rendering are:
  * KjcIndentStep
  * KjcNewlineString
  * KjcDefault
  * KjcSpaceBeforeColon
  * KjcNoSpaceAfterColon
  * KjcObjectStartOnNewLine
  * KjcArrayStartOnNewLine
  * KjcArraysOnOneLine
  * KjcShortArraysOnOneLine
  * KjcObjectsOnOneLine
  * KjcShortObjectsOnOneLine
  * KjcMinimized
  * KjcNumbersAsStrings
  * KjcUnsorted
  * KjcSorted
  * KjcSortedReverse
  * KjcShortArrayMaxLen
  * KjcShortObjectMaxLen

 
### The kjson tool
```
% kjson -u
kjson [-u (show this text)]
      [-t (trace levels for kjson utility)]
      [-T (global trace levels)]
      [-v (verbose mode for kjson utility)]
      [--klVerbose (verbose mode for klog library)]
      [--kjVerbose (verbose mode for kjson library)]
      [-V (global verbose mode)]
      [--fixme (turn on FIXME log)]
      [-i (spaces per indent)]
      [-ol (one-line output)]
      [-sbc (space before colon)]
      [-nsac (no space after colon)]
      [-onl (object start bracket on new line)]
      [-anl (array start bracket on new line)]
      [-aol (arrays on one line)
      [-saol (short arrays (80 rendered chars) on one line)
      [-saml <len> (change the length for an array to be considered short - 80 is default)]
      [-ool (objects on one line)
      [-sool (short objects (80 rendered chars) on one line)
      [-soml <len> (change the length for an object to be considered short - 80 is default)]
      [-min (minimized - no whitespace at all)]
      [-pn (do parse numbers)]
      [-sr (output to stderr)]
      [-o <output file>]
      [-sort (sort object members alphabetically)]
      [-rsort (sort object members in reverse alphabetic order)]
      [-saxTest (print a log line for each SAX event)]
      <json-file>
```

## License
[Apache 2.0](LICENSE) © 2017-2020 Ken Zangelin
