// 
// FILE            kjDefaults.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
//
// This file contains all the default values of the library, especially used when
// initializing the Kjson struct in kjInit and kjFree (if reuse is set to true).
//
#ifndef KJSON_KJDEFAULTS_H_
#define KJSON_KJDEFAULTS_H_


#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE



// -----------------------------------------------------------------------------
//
// Default values for the kjson library
//
#define  KJ_DEFAULT_ALLOC_SIZE                           (2 * 1024 * 1024)
#define  KJ_DEFAULT_MAX_INDENT_LEVEL                     50
#define  KJ_DEFAULT_SPACES_PER_INDENT                    2;
#define  KJ_DEFAULT_NL_STRING                            "\n";
#define  KJ_DEFAULT_STRING_BEFORE_COLON                  "";
#define  KJ_DEFAULT_STRING_AFTER_COLON                   " ";
#define  KJ_DEFAULT_OBJECT_START_BRACKET_ON_NEW_LINE     KFALSE
#define  KJ_DEFAULT_ARRAY_START_BRACKET_ON_NEW_LINE      KFALSE
#define  KJ_DEFAULT_NUMBERS_AS_STRINGS                   KFALSE    // TRUE only when beautifying
#define  KJ_DEFAULT_SHORT_ARRAY_MAX_LEN                  80
#define  KJ_DEFAULT_SHORT_OBJECT_MAX_LEN                 80
#define  KJ_DEFAULT_ADD_METHOD                           NULL      // inline append

#endif  // KJSON_KJDEFAULTS_H_
