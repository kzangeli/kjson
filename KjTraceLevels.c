// 
// FILE            KjTraceLevels.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "klog/KlComponent.h"                // KlComponentTraceLevelInfo

#include "kjson/kjTraceLevels.h"             // Own interface



// -----------------------------------------------------------------------------
//
// ktTraceLevelInfo
//
KlComponentTraceLevelInfo kjTraceLevelInfo[] =
{
  { KjlParse,           "Parsing"                    },
  { KjlParseValue,      "Parsing a VALUE"            },
  { KjlParseObject,     "Parsing an OBJECT"          },
  { KjlParseArray,      "Parsing an ARRAY"           },
  { KjlAllocBuffer,     "KJ Alloc Buffer"            },
  { KjlMalloc,          "Memory Allocation"          },
  { KjlAllocBytesLeft,  "Bytes Left in Alloc Buffer" },
  { KjlRender,          "Rendering a JSON"           },
  { KjlRenderNode,      "Rendering a Node"           },
  { KjlRenderArray,     "Rendering an Array"         },
  { KjlRenderObject,    "Rendering an Object"        },
  { KjlRenderValue,     "Rendering a Value"          },
  { KjlShortArray,      "Short Arrays"               },
  { KjlNewline,         "Newlines"                   }
};
