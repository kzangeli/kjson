// 
// FILE            kjLookup.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJSON_KJLOOKUP_H_
#define KJSON_KJLOOKUP_H_

#include "kjson/KjNode.h"               // KjNode



// -----------------------------------------------------------------------------
//
// kjLookup - look up a node named 'name' in container 'container'
//
// For containers of type KjObject only, the name of the nodes (children of the object)
// are compared tothe parameter 'name' and if equal, a pointer to the node is returned.
//
extern KjNode* kjLookup(KjNode* container, const char* name);
extern KjNode* kjLookupWithStrcmp(KjNode* container, char* name);

#ifdef USE_CHAR_SUM
extern KjNode* kjLookupWithCharSum(KjNode* container, char* name);
#endif

#endif  // KJSON_KJLOOKUP_H_
