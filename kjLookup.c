// 
// FILE            kjLookup.c - lookup a member of a container
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // NULL
#include <string.h>                     // strcmp

#include "kbase/kStrEq.h"               // kStrEq
#include "kbase/kLibLog.h"              // K Log macros

#include "kjson/KjTraceLevels.h"        // Kjl*
#include "kjson/KjNode.h"               // KjNode
#include "kjson/kjLookup.h"             // Own Interface



// -----------------------------------------------------------------------------
//
// kjLookup - look up a node named 'name' in container 'container'
//
// For containers of type KjObject only, the name of the nodes (children of the object)
// are compared tothe parameter 'name' and if equal, a pointer to the node is returned.
//
KjNode* kjLookup(KjNode* container, const char* name)
{
  if (name == NULL)
    return NULL;

  if (container->type == KjObject)
  {
    KjNode* current;

    for (current = container->value.firstChildP; current != NULL; current = current->next)
    {
      if (kStrEq(current->name, name) == KTRUE)
        return current;
    }
  }
  
  return NULL;
}



#ifdef USE_CHAR_SUM
// -----------------------------------------------------------------------------
//
// kjLookupWithCharSum -
//
KjNode* kjLookupWithCharSum(KjNode* container, char* name)
{
  if (name == NULL)
    return NULL;

  if (container->type == KjObject)
  {
    unsigned short cSum = 0;
    char*          s;
    KjNode*        current;

    for (s = name; *s != 0; s++)
      cSum += *s;

    for (current = container->value.firstChildP; current != NULL; current = current->next)
    {
      printf("Comparing '%s' with '%s' (cSums: %d, %d)\n", current->name, name, current->cSum, cSum);
      if ((current->cSum == cSum) && (kStrEq(current->name, name) == KTRUE))
        return current;
    }
  }
  
  return NULL;
}
#endif


// -----------------------------------------------------------------------------
//
// kjLookup - look up a node named 'name' in container 'container'
//
// For containers of type KjObject only, the name of the nodes (children of the object)
// are compared tothe parameter 'name' and if equal, a pointer to the node is returned.
//
KjNode* kjLookupWithStrcmp(KjNode* container, char* name)
{
  if (name == NULL)
    return NULL;

  if (container->type == KjObject)
  {
    KjNode* current;

    for (current = container->value.firstChildP; current != NULL; current = current->next)
    {
      if (strcmp(current->name, name) == 0)
        return current;
    }
  }
  
  return NULL;
}
