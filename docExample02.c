// 
// FILE            docExample02.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // printf
#include <strings.h>                    // bzero

#include "kalloc/kaBufferInit.h"        // kaBufferInit

#include "kjson/kjson.h"                // kjson lib header
#include "kjson/kjBuilder.h"            // kjson builder header
#include "kjson/kjConfig.h"
#include "kjson/kjParse.h"
#include "kjson/kjRender.h"
#include "kjson/kjBufferCreate.h"


static char jsonBuf[32 * 1024];
char kallocBuffer[128 * 1024];

// -----------------------------------------------------------------------------
//
// main - docExample02 - simple test program of libkjson
//
int main(int argC, char* argV[])
{
  Kjson   kj;
  Kjson*  kjP;
  KAlloc  kalloc;
  
  bzero(jsonBuf, sizeof(jsonBuf));
  kaBufferInit(&kalloc, kallocBuffer, sizeof(kallocBuffer), 16 * 1024, NULL, (char*) "KJSON Alloc Buffer");  
  kjP = kjBufferCreate(&kj, &kalloc);

  KjNode*  top  = kjObject(kjP, "top");
  KjNode*  a    = kjArray(kjP, "a");
  KjNode*  s1   = kjString(kjP, "s", "a string");
  KjNode*  i1   = kjInteger(kjP, "fourteen", 14);
  
  
  kjChildAdd(top, a);
  kjChildAdd(a, s1);
  kjChildAdd(a, i1);

  // Render the tree 
  char rBuf[1024];
  kjRender(kjP, top, rBuf, sizeof(rBuf));
  printf("%s\n", rBuf);

  return 0;
}
