// 
// FILE            kjBuilder.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KJ_BUILDER_H
#define KJ_BUILDER_H

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE
#include "kjson/kjson.h"                // Kjson struct



// -----------------------------------------------------------------------------
//
// KJSON builder functions - 
//
extern KjNode* kjObject(Kjson* kjP, const char* name);
extern KjNode* kjArray(Kjson* kjP, const char* name);
extern KjNode* kjString(Kjson* kjP, const char* name, const char* value);
extern KjNode* kjInteger(Kjson* kjP, const char* name, long long value);
extern KjNode* kjFloat(Kjson* kjP, const char* name, double value);
extern KjNode* kjNull(Kjson* kjP, const char* name);
extern KjNode* kjBoolean(Kjson* kjP, const char* name, KBool value);
extern void    kjChildRemove(KjNode* container, KjNode* child);
extern void    kjChildAdd(KjNode* container, KjNode* child);
extern void    kjChildAddSorted(KjNode* container, KjNode* child);
extern void    kjChildAddSortedReverse(KjNode* container, KjNode* child);

#endif
